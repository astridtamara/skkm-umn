<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_events".
 *
 * @property string $INSTITUTION
 * @property integer $UMN_AREA_ID
 * @property integer $UMN_EVENT_ID
 * @property string $DESCR
 * @property string $DESCR_X1
 * @property integer $POINTS
 *
 * @property PsUmnAreas $uMNAREA
 * @property PsUmnPntPerPct[] $psUmnPntPerPcts
 * @property PsUmnSkkmEntry[] $psUmnSkkmEntries
 * @property PsUmnStdntAcvty[] $psUmnStdntAcvties
 */
class PsUmnEvents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UMN_AREA_ID', 'UMN_EVENT_ID', 'DESCR', 'DESCR_X1'], 'required'],
            [['UMN_AREA_ID', 'UMN_EVENT_ID', 'POINTS'], 'integer'],
            [['INSTITUTION'], 'string', 'max' => 5],
            [['DESCR'], 'string', 'max' => 30],
            [['DESCR_X1'], 'string', 'max' => 140],
            [['UMN_EVENT_ID', 'UMN_AREA_ID'], 'unique', 'targetAttribute' => ['UMN_EVENT_ID', 'UMN_AREA_ID'], 'message' => 'The combination of Umn  Area  ID and Umn  Event  ID has already been taken.'],
            [['UMN_AREA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnAreas::className(), 'targetAttribute' => ['UMN_AREA_ID' => 'UMN_AREA_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INSTITUTION' => 'Institution',
            'UMN_AREA_ID' => 'UMN  Area  ID',
            'UMN_EVENT_ID' => 'UMN  Event  ID',
            'DESCR' => 'Event',
            'DESCR_X1' => 'Event',
            'POINTS' => 'Points',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNAREA()
    {
        return $this->hasOne(PsUmnAreas::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnPntPerPcts()
    {
        return $this->hasMany(PsUmnPntPerPct::className(), ['UMN_EVENT_ID' => 'UMN_EVENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnSkkmEntries()
    {
        return $this->hasMany(PsUmnSkkmEntry::className(), ['UMN_EVENT_ID' => 'UMN_EVENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnStdntAcvties()
    {
        return $this->hasMany(PsUmnStdntAcvty::className(), ['UMN_EVENT_ID' => 'UMN_EVENT_ID']);
    }
}
