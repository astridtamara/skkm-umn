<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_stdnt_vw".
 *
 * @property string $EMPLID
 * @property string $NM
 *
 * @property PsUmnStdntAcvty[] $psUmnStdntAcvties
 */
class PsUmnStdntVw extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_stdnt_vw';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EMPLID', 'NM'], 'required'],
            [['EMPLID'], 'string', 'max' => 11],
            [['NM'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EMPLID' => 'NIM',
            'NM' => 'Nama Mahasiswa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnStdntAcvties()
    {
        return $this->hasMany(PsUmnStdntAcvty::className(), ['EMPLID' => 'EMPLID']);
    }
}
