<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_parpnt_type".
 *
 * @property string $INSTITUTION
 * @property integer $UMN_PARTICIPANT_ID
 * @property string $DESCR
 * @property string $DESCRSHORT
 *
 * @property PsUmnSkkmEntry $iNSTITUTION
 * @property PsUmnPntPerPct[] $psUmnPntPerPcts
 * @property PsUmnStdntAcvty[] $psUmnStdntAcvties
 */
class PsUmnParpntType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_parpnt_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DESCR', 'DESCRSHORT'], 'required'],
            [['INSTITUTION'], 'string', 'max' => 5],
            [['DESCR'], 'string', 'max' => 30],
            [['DESCRSHORT'], 'string', 'max' => 10],
            [['INSTITUTION'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnSkkmEntry::className(), 'targetAttribute' => ['INSTITUTION' => 'INSTITUTION']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INSTITUTION' => 'Institution',
            'UMN_PARTICIPANT_ID' => 'UMN  Participant  ID',
            'DESCR' => 'Participant Type',
            'DESCRSHORT' => 'Participant Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getINSTITUTION()
    {
        return $this->hasOne(PsUmnSkkmEntry::className(), ['INSTITUTION' => 'INSTITUTION']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnPntPerPcts()
    {
        return $this->hasMany(PsUmnPntPerPct::className(), ['UMN_PARTICIPANT_ID' => 'UMN_PARTICIPANT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnStdntAcvties()
    {
        return $this->hasMany(PsUmnStdntAcvty::className(), ['UMN_PARTICIPANT_ID' => 'UMN_PARTICIPANT_ID']);
    }
}
