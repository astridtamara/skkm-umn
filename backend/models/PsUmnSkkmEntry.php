<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_skkm_entry".
 *
 * @property integer $SEQNUM_DAY1
 * @property integer $UMN_AREA_ID
 * @property integer $UMN_EVENT_ID
 * @property integer $UMN_LEVEL_ID
 * @property string $INSTITUTION
 * @property string $DESCR_80
 * @property string $DESCR50_1
 * @property string $START_DATE
 * @property string $END_DATE
 * @property integer $COUNT_TOTAL
 *
 * @property Panitia $panitia
 * @property PsUmnParpntType[] $psUmnParpntTypes
 * @property PsUmnPntPerPct[] $psUmnPntPerPcts
 * @property PsUmnAreas $uMNAREA
 * @property PsUmnEvents $uMNEVENT
 * @property PsUmnLevels $uMNLEVEL
 * @property PsUmnStdntAcvty[] $psUmnStdntAcvties
 * @property PsUmnStdntVw[] $eMPLs
 */
class PsUmnSkkmEntry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_skkm_entry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'DESCR_80', 'DESCR50_1', 'START_DATE', 'END_DATE', 'COUNT_TOTAL'], 'required'],
            [['UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'COUNT_TOTAL'], 'integer'],
            [['START_DATE', 'END_DATE'], 'safe'],
            [['INSTITUTION'], 'string', 'max' => 5],
            [['DESCR_80'], 'string', 'max' => 80],
            [['DESCR50_1'], 'string', 'max' => 50],
            [['UMN_AREA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnAreas::className(), 'targetAttribute' => ['UMN_AREA_ID' => 'UMN_AREA_ID']],
            [['UMN_EVENT_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnEvents::className(), 'targetAttribute' => ['UMN_EVENT_ID' => 'UMN_EVENT_ID']],
            [['UMN_LEVEL_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnLevels::className(), 'targetAttribute' => ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INSTITUTION' => 'Institution',
            'UMN_AREA_ID' => 'Nama bidang',
            'UMN_EVENT_ID' => 'Nama kegiatan',
            'DESCR_80' => 'Nama Aktivitas',
            'DESCR50_1' => 'Nama Tempat',
            'START_DATE' => 'Tanggal mulai',
            'END_DATE' => 'Tanggal selesai',
            'UMN_LEVEL_ID' => 'Level penyelenggaraan',
            'COUNT_TOTAL' => 'Jumlah presensi yg dibutuhkan',
            'SEQNUM_DAY1' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPanitia()
    {
        return $this->hasOne(Panitia::className(), ['SEQNUM_DAY1' => 'SEQNUM_DAY1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnParpntTypes()
    {
        return $this->hasMany(PsUmnParpntType::className(), ['INSTITUTION' => 'INSTITUTION']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnPntPerPcts()
    {
        return $this->hasMany(PsUmnPntPerPct::className(), ['INSTITUTION' => 'INSTITUTION']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNAREA()
    {
        return $this->hasOne(PsUmnAreas::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNEVENT()
    {
        return $this->hasOne(PsUmnEvents::className(), ['UMN_EVENT_ID' => 'UMN_EVENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNLEVEL()
    {
        return $this->hasOne(PsUmnLevels::className(), ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnStdntAcvties()
    {
        return $this->hasMany(PsUmnStdntAcvty::className(), ['SEQNUM_DAY1' => 'SEQNUM_DAY1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEMPLs()
    {
        return $this->hasMany(PsUmnStdntVw::className(), ['EMPLID' => 'EMPLID'])->viaTable('ps_umn_stdnt_acvty', ['SEQNUM_DAY1' => 'SEQNUM_DAY1']);
    }
}
