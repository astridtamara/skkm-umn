<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_stdnt_acvty".
 *
 * @property string $INSTITUTION
 * @property integer $UMN_AREA_ID
 * @property integer $UMN_EVENT_ID
 * @property integer $UMN_LEVEL_ID
 * @property integer $SEQNUM_DAY1
 * @property string $EMPLID
 * @property integer $UMN_PARTICIPANT_ID
 * @property integer $COUNT_TOTAL
 * @property integer $UMN_OBTAIN_POINTS
 * @property string $ATTEND_PRESENT
 *
 * @property PsUmnAreas $uMNAREA
 * @property PsUmnEvents $uMNEVENT
 * @property PsUmnSkkmEntry $sEQNUMDAY1
 * @property PsUmnLevels $uMNLEVEL
 * @property PsUmnParpntType $uMNPARTICIPANT
 * @property PsUmnStdntVw $eMPL
 */
class PsUmnStdntAcvty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_stdnt_acvty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['INSTITUTION', 'UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'SEQNUM_DAY1', 'EMPLID', 'UMN_PARTICIPANT_ID'], 'required'],
            [['UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'SEQNUM_DAY1', 'UMN_PARTICIPANT_ID', 'COUNT_TOTAL', 'UMN_OBTAIN_POINTS'], 'integer'],
            [['ATTEND_PRESENT'], 'string'],
            [['INSTITUTION'], 'string', 'max' => 5],
            [['EMPLID'], 'string', 'max' => 11],
            // [['SEQNUM_DAY1'], 'unique'],
            [['UMN_AREA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnAreas::className(), 'targetAttribute' => ['UMN_AREA_ID' => 'UMN_AREA_ID']],
            [['UMN_EVENT_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnEvents::className(), 'targetAttribute' => ['UMN_EVENT_ID' => 'UMN_EVENT_ID']],
            [['SEQNUM_DAY1'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnSkkmEntry::className(), 'targetAttribute' => ['SEQNUM_DAY1' => 'SEQNUM_DAY1']],
            [['UMN_LEVEL_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnLevels::className(), 'targetAttribute' => ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']],
            [['UMN_PARTICIPANT_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnParpntType::className(), 'targetAttribute' => ['UMN_PARTICIPANT_ID' => 'UMN_PARTICIPANT_ID']],
            [['EMPLID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnStdntVw::className(), 'targetAttribute' => ['EMPLID' => 'EMPLID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INSTITUTION' => 'INSTITUTION',
            'UMN_AREA_ID' => 'Nama bidang',
            'UMN_EVENT_ID' => 'Nama kegiatan',
            'UMN_LEVEL_ID' => 'Level penyelenggaraan',
            'SEQNUM_DAY1' => 'Event',
            'EMPLID' => 'NIM mahasiswa',
            'UMN_PARTICIPANT_ID' => 'Tipe partisipan',
            'COUNT_TOTAL' => 'Jumlah Kehadiran',
            'UMN_OBTAIN_POINTS' => 'UMN OBTAIN POINTS',
            'ATTEND_PRESENT' => 'Point SKKM',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNAREA()
    {
        return $this->hasOne(PsUmnAreas::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNEVENT()
    {
        return $this->hasOne(PsUmnEvents::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID','UMN_EVENT_ID' => 'UMN_EVENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNSkkmEntry()
    {
        return $this->hasOne(PsUmnSkkmEntry::className(), ['SEQNUM_DAY1' => 'SEQNUM_DAY1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNLEVEL()
    {
        return $this->hasOne(PsUmnLevels::className(), ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNPARTICIPANT()
    {
        return $this->hasOne(PsUmnParpntType::className(), ['UMN_PARTICIPANT_ID' => 'UMN_PARTICIPANT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswa()
    {
        return $this->hasOne(PsUmnStdntVw::className(), ['EMPLID' => 'EMPLID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnPntPerPcts()
    {
        return $this->hasOne(PsUmnPntPerPct::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID','UMN_EVENT_ID' => 'UMN_EVENT_ID','UMN_LEVEL_ID' => 'UMN_LEVEL_ID','UMN_PARTICIPANT_ID' => 'UMN_PARTICIPANT_ID']);
    }
}
