<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PsUmnStdntAcvty;

/**
 * SkkmStudentSearch represents the model behind the search form about `backend\models\UMNStudentSKKM`.
 */
class SkkmStudentSearch extends PsUmnStdntAcvty
{
    public $Nama;
    public $Points;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['INSTITUTION', 'EMPLID', 'Nama', 'ATTEND_PRESENT', 'SEQNUM_DAY1', 'UMN_PARTICIPANT_ID'], 'safe'],
            [['UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'COUNT_TOTAL', 'UMN_OBTAIN_POINTS', 'Points'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsUmnStdntAcvty::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['uMNAREA']);
        $query->joinWith(['uMNEVENT']);
        $query->joinWith(['uMNLEVEL']);
        $query->joinWith(['uMNSkkmEntry']);
        $query->joinWith(['uMNPARTICIPANT']);
        $query->joinWith(['mahasiswa']);
        $query->joinWith(['psUmnPntPerPcts']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes'=>[
                'Nama'=>[
                    'asc'=>['ps_umn_stdnt_vw.NM'=>SORT_ASC],
                    'desc'=>['ps_umn_stdnt_vw.NM'=>SORT_DESC],
                ],
                'Points'=>[
                    'asc'=>['ps_umn_pnt_per_pct.POINTS'=>SORT_ASC],
                    'desc'=>['ps_umn_pnt_per_pct.POINTS'=>SORT_DESC],
                ],
                'COUNT_TOTAL'=>[
                    'asc'=>['ps_umn_stdnt_acvty.COUNT_TOTAL'=>SORT_ASC],
                    'desc'=>['ps_umn_stdnt_acvty.COUNT_TOTAL'=>SORT_DESC],
                ],
                'CREATE_DATE'=>[
                    'asc'=>['ps_umn_stdnt_acvty.CREATE_DATE'=>SORT_ASC],
                    'desc'=>['ps_umn_stdnt_acvty.CREATE_DATE'=>SORT_DESC],
                ],
                'ATTEND_PRESENT'=>[
                    'asc'=>['ps_umn_stdnt_acvty.ATTEND_PRESENT'=>SORT_ASC],
                    'desc'=>['ps_umn_stdnt_acvty.ATTEND_PRESENT'=>SORT_DESC],
                ],
                'EMPLID'=>[
                    'asc'=>['EMPLID'=>SORT_ASC],
                    'desc'=>['EMPLID'=>SORT_DESC],
                ],
                'SEQNUM_DAY1'=>[
                    'asc'=>['ps_umn_skkm_entry.DESCR_80'=>SORT_ASC],
                    'desc'=>['ps_umn_skkm_entry.DESCR_80'=>SORT_DESC],
                ],
                'UMN_PARTICIPANT_ID'=>[
                    'asc'=>['ps_umn_parpnt_type.DESCR'=>SORT_ASC],
                    'desc'=>['ps_umn_parpnt_type.DESCR'=>SORT_DESC],
                ],
                // 'UMN_EVENT_ID'=>[
                //     'asc'=>['ps_umn_events.POINTS'=>SORT_ASC],
                //     'desc'=>['ps_umn_events.POINTS'=>SORT_DESC],
                // ],

            ]
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'EMPLID' => $this->EMPLID,
            'UMN_LEVEL_ID' => $this->UMN_LEVEL_ID,
            'UMN_PARTICIPANT_ID' => $this->UMN_PARTICIPANT_ID,
            'UMN_AREA_ID' => $this->UMN_AREA_ID,
            // 'UMN_EVENT_ID' => $this->UMN_EVENT_ID,
            'ps_umn_stdnt_acvty.SEQNUM_DAY1' => $this->SEQNUM_DAY1,
            'ps_umn_stdnt_acvty.COUNT_TOTAL' => $this->COUNT_TOTAL,
            // 'UMN_OBTAIN_POINTS' => $this->UMN_OBTAIN_POINTS,
        ]);

        $query->andFilterWhere(['like', 'EMPLID', $this->EMPLID])
            ->andFilterWhere(['like', 'ps_umn_stdnt_vw.NM', $this->Nama])
            ->andFilterWhere(['like', 'ps_umn_pnt_per_pct.POINTS', $this->Points])
            // ->andFilterWhere(['like', 'ps_umn_areas.DESCR', $this->UMN_AREA_ID])
            // ->andFilterWhere(['like', 'ps_umn_levels.DESCR', $this->UMN_LEVEL_ID])
            // ->andFilterWhere(['like', 'ps_umn_skkm_entry.DESCR_80', $this->SEQNUM_DAY1])
            ->andFilterWhere(['like', 'ps_umn_parpnt_type.DESCR', $this->UMN_PARTICIPANT_ID])
            // ->andFilterWhere(['like', 'ps_umn_events.POINTS', $this->UMN_EVENT_ID])
            ->andFilterWhere(['like', 'ATTEND_PRESENT', $this->ATTEND_PRESENT])
            ;
            // print_r('<br><br><br><br><br><br>');
// var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        return $dataProvider;
    }
}
