<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "panitia".
 *
 * @property integer $SEQNUM_DAY1
 * @property integer $UserID
 * @property string $PKey
 *
 * @property User $user
 * @property PsUmnSkkmEntry $sEQNUMDAY1
 */
class Panitia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'panitia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SEQNUM_DAY1', 'UserID'], 'required'],
            [['SEQNUM_DAY1', 'UserID'], 'integer'],
            [['PKey'], 'string', 'max' => 15],
            [['UserID'], 'unique'],
            [['SEQNUM_DAY1'], 'unique'],
            [['PKey'], 'unique'],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'id']],
            [['SEQNUM_DAY1'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnSkkmEntry::className(), 'targetAttribute' => ['SEQNUM_DAY1' => 'SEQNUM_DAY1']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SEQNUM_DAY1' => 'ID Acara',
            'UserID' => 'ID User',
            'PKey' => 'Password',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'UserID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSEQNUMDAY1()
    {
        return $this->hasOne(PsUmnSkkmEntry::className(), ['SEQNUM_DAY1' => 'SEQNUM_DAY1']);
    }

    public function signup($username)
    {
        $user = new User();
        $user->username = $username;
        $user->setPassword($this->PKey);
        $user->generateAuthKey();
        $user->save();
        $this->UserID = $user->getId();
        // return  $user;
    }
}
