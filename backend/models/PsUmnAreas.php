<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_areas".
 *
 * @property string $INSTITUTION
 * @property integer $UMN_AREA_ID
 * @property string $DESCR
 * @property string $DESCRFORMAL
 * @property string $DESCRSHORT
 *
 * @property PsUmnPntPerPct $uMNAREA
 * @property PsUmnEvents[] $psUmnEvents
 * @property PsUmnPntPerPct[] $psUmnPntPerPcts
 * @property PsUmnSkkmEntry[] $psUmnSkkmEntries
 * @property PsUmnStdntAcvty[] $psUmnStdntAcvties
 */
class PsUmnAreas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_areas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UMN_AREA_ID', 'DESCR', 'DESCRFORMAL', 'DESCRSHORT'], 'required'],
            [['UMN_AREA_ID'], 'integer'],
            [['INSTITUTION'], 'string', 'max' => 5],
            [['DESCR'], 'string', 'max' => 30],
            [['DESCRFORMAL'], 'string', 'max' => 50],
            [['DESCRSHORT'], 'string', 'max' => 10],
            [['UMN_AREA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnPntPerPct::className(), 'targetAttribute' => ['UMN_AREA_ID' => 'UMN_AREA_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INSTITUTION' => 'Institution',
            'UMN_AREA_ID' => 'UMN  Area  ID',
            'DESCR' => 'Area',
            'DESCRFORMAL' => 'Area',
            'DESCRSHORT' => 'Area',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNAREA()
    {
        return $this->hasOne(PsUmnPntPerPct::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnEvents()
    {
        return $this->hasMany(PsUmnEvents::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnPntPerPcts()
    {
        return $this->hasMany(PsUmnPntPerPct::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnSkkmEntries()
    {
        return $this->hasMany(PsUmnSkkmEntry::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnStdntAcvties()
    {
        return $this->hasMany(PsUmnStdntAcvty::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }
}
