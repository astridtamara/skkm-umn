<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PsUmnSkkmEntry;

/**
 * SkkmSearch represents the model behind the search form about `backend\models\PsUmnSkkmEntry`.
 */
class SkkmSearch extends PsUmnSkkmEntry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SEQNUM_DAY1', 'COUNT_TOTAL'], 'integer'],
            [['INSTITUTION', 'UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'DESCR_80', 'DESCR50_1', 'START_DATE', 'END_DATE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PsUmnSkkmEntry::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'pagination' => [
            //     'pageSize' => 50,
            // ],
        ]);

        $query->joinWith(['uMNAREA']);
        // $query->joinWith(['uMNEVENT']);
        $query->joinWith(['uMNLEVEL']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes'=>[
                'DESCR_80'=>[
                    'asc'=>['DESCR_80'=>SORT_ASC],
                    'desc'=>['DESCR_80'=>SORT_DESC],
                ],
                'UMN_AREA_ID'=>[
                    'asc'=>['ps_umn_areas.DESCR'=>SORT_ASC],
                    'desc'=>['ps_umn_areas.DESCR'=>SORT_DESC],
                ],
                'UMN_EVENT_ID',
                // 'UMN_EVENT_ID'=>[
                //     'asc'=>['ps_umn_events.DESCR'=>SORT_ASC],
                //     'desc'=>['ps_umn_events.DESCR'=>SORT_DESC],
                // ],
                'UMN_LEVEL_ID'=>[
                    'asc'=>['ps_umn_levels.DESCR'=>SORT_ASC],
                    'desc'=>['ps_umn_levels.DESCR'=>SORT_DESC],
                ],
                'START_DATE'=>[
                    'asc'=>['START_DATE'=>SORT_ASC],
                    'desc'=>['START_DATE'=>SORT_DESC],
                ],
                'END_DATE'=>[
                    'asc'=>['END_DATE'=>SORT_ASC],
                    'desc'=>['END_DATE'=>SORT_DESC],
                ],
            ]
        ]);
        // grid filtering conditions
        $query->andFilterWhere([
            // 'UMN_AREA_ID' => $this->UMN_AREA_ID,
            // 'UMN_EVENT_ID' => $this->UMN_EVENT_ID,
            // 'UMN_LEVEL_ID' => $this->UMN_LEVEL_ID,
            'START_DATE' => $this->START_DATE,
            'END_DATE' => $this->END_DATE,
            'COUNT_TOTAL' => $this->COUNT_TOTAL,
            'SEQNUM_DAY1' => $this->SEQNUM_DAY1,
        ]);

        $query->andFilterWhere(['like', 'DESCR_80', $this->DESCR_80])
            // ->andFilterWhere(['like', 'INSTITUTION', $this->INSTITUTION])            
            // ->andFilterWhere(['like', 'DESCR50_1', $this->DESCR50_1])
            ->andFilterWhere(['like', 'ps_umn_areas.DESCR', $this->UMN_AREA_ID])
            // ->andFilterWhere(['like', 'ps_umn_events.DESCR', $this->UMN_EVENT_ID])
            ->andFilterWhere(['like', 'ps_umn_levels.DESCR', $this->UMN_LEVEL_ID])
            ;

//             print_r('<br><br><br><br><br><br>');
// var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        return $dataProvider;
    }
}
