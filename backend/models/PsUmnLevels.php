<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_levels".
 *
 * @property string $INSTITUTION
 * @property integer $UMN_LEVEL_ID
 * @property string $DESCR
 * @property string $DESCRSHORT
 *
 * @property PsUmnPntPerPct[] $psUmnPntPerPcts
 * @property PsUmnSkkmEntry[] $psUmnSkkmEntries
 * @property PsUmnStdntAcvty[] $psUmnStdntAcvties
 */
class PsUmnLevels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_levels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['INSTITUTION', 'UMN_LEVEL_ID', 'DESCR', 'DESCRSHORT'], 'required'],
            [['UMN_LEVEL_ID'], 'integer'],
            [['INSTITUTION'], 'string', 'max' => 5],
            [['DESCR'], 'string', 'max' => 30],
            [['DESCRSHORT'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INSTITUTION' => 'Institution',
            'UMN_LEVEL_ID' => 'ID level penyelenggaraan',
            'DESCR' => 'Level Penyelenggaraan',
            'DESCRSHORT' => 'Descrshort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnPntPerPcts()
    {
        return $this->hasMany(PsUmnPntPerPct::className(), ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnSkkmEntries()
    {
        return $this->hasMany(PsUmnSkkmEntry::className(), ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnStdntAcvties()
    {
        return $this->hasMany(PsUmnStdntAcvty::className(), ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']);
    }
}
