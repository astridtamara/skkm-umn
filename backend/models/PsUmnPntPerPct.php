<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ps_umn_pnt_per_pct".
 *
 * @property string $INSTITUTION
 * @property integer $UMN_AREA_ID
 * @property integer $UMN_EVENT_ID
 * @property integer $UMN_LEVEL_ID
 * @property string $EFFDT
 * @property integer $UMN_PARTICIPANT_ID
 * @property integer $POINTS
 *
 * @property PsUmnAreas $psUmnAreas
 * @property PsUmnAreas $uMNAREA
 * @property PsUmnEvents $uMNEVENT
 * @property PsUmnLevels $uMNLEVEL
 * @property PsUmnSkkmEntry $iNSTITUTION
 * @property PsUmnParpntType $uMNPARTICIPANT
 */
class PsUmnPntPerPct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_umn_pnt_per_pct';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['INSTITUTION', 'UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'EFFDT', 'UMN_PARTICIPANT_ID', 'POINTS'], 'required'],
            [['UMN_AREA_ID', 'UMN_EVENT_ID', 'UMN_LEVEL_ID', 'UMN_PARTICIPANT_ID', 'POINTS'], 'integer'],
            [['EFFDT'], 'safe'],
            [['INSTITUTION'], 'string', 'max' => 5],
            [['UMN_AREA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnAreas::className(), 'targetAttribute' => ['UMN_AREA_ID' => 'UMN_AREA_ID']],
            [['UMN_EVENT_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnEvents::className(), 'targetAttribute' => ['UMN_EVENT_ID' => 'UMN_EVENT_ID']],
            [['UMN_LEVEL_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnLevels::className(), 'targetAttribute' => ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']],
            [['INSTITUTION'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnSkkmEntry::className(), 'targetAttribute' => ['INSTITUTION' => 'INSTITUTION']],
            [['UMN_PARTICIPANT_ID'], 'exist', 'skipOnError' => true, 'targetClass' => PsUmnParpntType::className(), 'targetAttribute' => ['UMN_PARTICIPANT_ID' => 'UMN_PARTICIPANT_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'INSTITUTION' => 'Institution',
            'UMN_AREA_ID' => 'UMN  Area  ID',
            'UMN_EVENT_ID' => 'UMN  Event  ID',
            'UMN_LEVEL_ID' => 'UMN  Level  ID',
            'EFFDT' => 'Date',
            'UMN_PARTICIPANT_ID' => 'UMN  Participant  ID',
            'POINTS' => 'Points',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPsUmnAreas()
    {
        return $this->hasOne(PsUmnAreas::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNAREA()
    {
        return $this->hasOne(PsUmnAreas::className(), ['UMN_AREA_ID' => 'UMN_AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNEVENT()
    {
        return $this->hasOne(PsUmnEvents::className(), ['UMN_EVENT_ID' => 'UMN_EVENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNLEVEL()
    {
        return $this->hasOne(PsUmnLevels::className(), ['UMN_LEVEL_ID' => 'UMN_LEVEL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getINSTITUTION()
    {
        return $this->hasOne(PsUmnSkkmEntry::className(), ['INSTITUTION' => 'INSTITUTION']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUMNPARTICIPANT()
    {
        return $this->hasOne(PsUmnParpntType::className(), ['UMN_PARTICIPANT_ID' => 'UMN_PARTICIPANT_ID']);
    }
}
