<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use kartik\icons\Icon;
use kartik\grid\GridView;
use kartik\editable\Editable;
use kartik\widgets\Select2;
use backend\models\PsUmnParpntType;
use backend\models\PsUmnPntPerPct;
use backend\models\PsUmnSkkmEntry;
// Initialize a specific framework - e.g. Web Hosting Hub Glyphs 
                            Icon::map($this, Icon::BSG);
/* @var $this yii\web\View */
/* @var $model backend\models\UMNSkkmEntry */
$this->title = $model->DESCR_80;
$this->params['breadcrumbs'][] = ['label' => 'SKKM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="umnskkm-entry-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if($PKey!='0'){?>
            <?= Html::a('Create QR Code', ['/site/create', 'id' => $model->SEQNUM_DAY1], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Import Excel peserta', ['/site/import', 'id' => $model->SEQNUM_DAY1], ['class' => 'btn btn-success']) ?>
            <!-- ?= Html::a('Update My UMN', ['', 'id' => $model->SEQNUM_DAY1], ['class' => 'btn btn-warning']) ?> -->
        <?php 
        }
        else {?>
            <?= Html::a('Create Username & Password', ['create', 'id' => $model->SEQNUM_DAY1], ['class' => 'btn btn-primary']) ?>
        <?php 
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'INSTITUTION',
            'SEQNUM_DAY1',
            'DESCR_80',
            'DESCR50_1',
            'uMNAREA.DESCR',
            'uMNEVENT.DESCR',
            'uMNLEVEL.DESCR',
            'START_DATE',
            'END_DATE',
            'COUNT_TOTAL',
            [
            'label' => 'Username',
            'value' => $username,
            ],
            'panitia.PKey',
        ],
    ]) ?>

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>


    <!-- <h1>?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        ?= Html::a('Create UMN Student SKKM', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'INSTITUTION',
            'EMPLID',
            [
                'attribute' => 'Nama',
                'label' => 'Nama Mahasiswa',
                'value' => 'mahasiswa.NM',
            ],
            // 'uMNAREA.DESCR',
            // 'uMNEVENT.DESCR',
            // 'uMNLEVEL.DESCR',
            // 'uMNSkkmEntry.DESCR_80',
            // 'uMNPARTICIPANT.DESCR',
            // 'UMN_OBTAIN_POINTS',
            // 'ATTEND_PRESENT',
            // 'uMNEVENT.POINTS',
            // [
            //     // 'label' => 'Events',
            //     'attribute' => 'SEQNUM_DAY1',
            //     'value' => 'uMNSkkmEntry.DESCR_80',
            // ],
            [
                'attribute' => 'UMN_PARTICIPANT_ID',
                // 'filter' => ArrayHelper::map(PsUmnParpntType::find()->orderBy(['DESCR'=>SORT_ASC])->all(),'UMN_PARTICIPANT_ID','DESCR'),
                'value' => 'uMNPARTICIPANT.DESCR',
                        //menggunakan widget untuk editable column
                        'class' => 'kartik\grid\EditableColumn',
                        //list data dropdown dari DB
                        'editableOptions' => [
                            'header' => 'tipe partisipan',
                            'asPopover' => false,
                            'format' => Editable::FORMAT_BUTTON,
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'inputType' => '\kartik\select2\Select2',
                            'options'=>
                            [
                                'data' => ArrayHelper::map(PsUmnParpntType::find()->all(),'UMN_PARTICIPANT_ID', 'DESCR'),
                            ],
                        ],
            ],
            [
                'label' => 'Points',
                'attribute' => 'Points',
                // 'filterInputOptions' => 'number',
                // 'filterOptions' => [ 'type' => 'number'],
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'type' => 'number'
                ],
                'value' => function($data){
                    if ($text = PsUmnPntPerPct::findOne(['UMN_AREA_ID' => $data['UMN_AREA_ID'],'UMN_EVENT_ID' => $data['UMN_EVENT_ID'],'UMN_LEVEL_ID' => $data['UMN_LEVEL_ID'],'UMN_PARTICIPANT_ID' => $data['UMN_PARTICIPANT_ID']])->POINTS) {
                        return $text;
                    }
                    else return 0;
                        },   
            ],
            [
                'attribute' => 'COUNT_TOTAL',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'type' => 'number'
                ],
            ],
            [
                'label' => 'Status',
                'attribute' => 'ATTEND_PRESENT',
                'format' => 'html',
                'filter' => [ 'Y' => 'Yes', 'N' => 'No'],
                'value' => function($data){
                            if ($data['ATTEND_PRESENT'] == 'Y') {
                                return Icon::show('ok', ['aria-hidden'=>"true",'style'=>"color:green;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                            } else {
                                return Icon::show('remove', ['aria-hidden'=>"true",'style'=>"color:red;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                            }
                        },   
                
            ],
            [
                'label' => 'On The Spot',
                'attribute' => 'CREATE_DATE',
                'format' => 'html',
                // 'filter' => [ 'Y' => 'Yes', 'N' => 'No'],
                'value' => function($data){
                    // $expression = new Expression('NOW()');
                    // $now = (new \yii\db\Query)->select($expression)->scalar();  // SELECT NOW();
                    $Event=PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $data['SEQNUM_DAY1']])->one();
                            if ($data['CREATE_DATE'] <= $Event->START_DATE) {
                                return Icon::show('remove', ['aria-hidden'=>"true",'style'=>"color:red;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                            } else {
                                return Icon::show('ok', ['aria-hidden'=>"true",'style'=>"color:green;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                            }
                        },   
                
            ],
            // 'ATTEND_PRESENT',
            [   
                'class' => 'yii\grid\ActionColumn', 
                'template' => '{view} {update}',
                'controller' => 'skkm-student'     
            ],
        ],
    ]); ?>

</div>
