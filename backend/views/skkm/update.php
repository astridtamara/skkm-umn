<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PsUmnSkkmEntry */

$this->title = 'Update Ps Umn Skkm Entry: ' . $model->SEQNUM_DAY1;
$this->params['breadcrumbs'][] = ['label' => 'Ps Umn Skkm Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SEQNUM_DAY1, 'url' => ['view', 'id' => $model->SEQNUM_DAY1]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ps-umn-skkm-entry-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
