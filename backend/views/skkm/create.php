<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PsUmnSkkmEntry */

$this->title = 'Create Ps Umn Skkm Entry';
$this->params['breadcrumbs'][] = ['label' => 'Ps Umn Skkm Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ps-umn-skkm-entry-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
