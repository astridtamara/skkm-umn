<?php

use yii\helpers\Html;
// use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\UMNSkkmEntry */
// $this->title = $model->SEQNUM_DAY1;
$this->title = 'Create QR Code';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['event']=$event->SEQNUM_DAY1;
?>
<div class="site-create">
    <h1><?= Html::encode($this->title) . " " . $event->DESCR_80 ;?> </h1>
        <?= Html::a('Download QR Code', ['download', 'id' => $event->SEQNUM_DAY1], ['class' => 'btn btn-primary']) ?>
    <?=
    // $this->context->ImportExcel();
                GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
            [
                'attribute' => 'nim',
                'label' => 'NIM Mahasiswa',
            ],
                    [
                        'attribute' => 'nim',
                        'format' => 'html',
                        'label' => 'QR Code',
                        'value' => function ($data) {
                            return Html::img('@web/assets/QR/temp/' . Yii::$app->params['event'] . '/' . $data['nim'].'.png' , ['class' => 'img-responsive center','width' => '60px']);
                        },
                    ],
                ],
            ]);
    // foreach ($dataProvider as $key) {
    //     echo $key."<br>";
    // }
    // print_r($dataProvider);
            ?>

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
