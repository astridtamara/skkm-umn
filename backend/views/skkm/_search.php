<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SkkmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="umnskkm-entry-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'INSTITUTION') ?>

    <?= $form->field($model, 'UMN_AREA_ID') ?>

    <?= $form->field($model, 'UMN_EVENT_ID') ?>

    <?= $form->field($model, 'DESCR_80') ?>

    <?= $form->field($model, 'DESCR50_1') ?>

    <?php // echo $form->field($model, 'START_DATE') ?>

    <?php // echo $form->field($model, 'END_DATE') ?>

    <?php // echo $form->field($model, 'UMN_LEVEL_ID') ?>

    <?php // echo $form->field($model, 'COUNT_TOTAL') ?>

    <?php // echo $form->field($model, 'SEQNUM_DAY1') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
