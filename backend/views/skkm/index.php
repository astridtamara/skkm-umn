<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SkkmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SKKM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="umnskkm-entry-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'INSTITUTION',
            'DESCR_80',
            // 'DESCR50_1',
            [
                'attribute' => 'UMN_AREA_ID',
                'value' => 'uMNAREA.DESCR',
            ],
            [
                'attribute' => 'UMN_EVENT_ID',
                'value' => 'uMNEVENT.DESCR',
            ],
            [
                'attribute' => 'UMN_LEVEL_ID',
                'value' => 'uMNLEVEL.DESCR',
            ],
            // 'uMNAREA.DESCR',
            // 'uMNEVENT.DESCR',
            // 'uMNLEVEL.DESCR',
            'START_DATE',
            'END_DATE',
            // 'COUNT_TOTAL',
            // 'SEQNUM_DAY1',

            // ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>
     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
