<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
        use yii\helpers\Url;
use backend\models\PsUmnAreas;
use backend\models\PsUmnEvents;
use backend\models\PsUmnLevels;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\UMNSkkmEntry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="umnskkm-entry-form">

    <?php $form = ActiveForm::begin(); 

        $UMNArea = ArrayHelper::map(PsUmnAreas::find()->all(), 'UMN_AREA_ID', 'DESCR');
        echo $form->field($model, 'UMN_AREA_ID')->dropDownList(
                $UMNArea,
                [
                    'prompt'=>'Select Area',
                    'onchange'=>'
                        $.get( "'.Url::toRoute('/skkm/lists').'", { id: $(this).val() } )
                            .done(function( data ) {
                                $( "#'.Html::getInputId($model, 'UMN_EVENT_ID').'" ).html( data );
                            }
                        );
                    '    
                ]
        ); 

     echo $form->field($model, 'UMN_EVENT_ID')->dropDownList(['prompt'=>'Select Event']);

        ?>

    <?= $form->field($model, 'UMN_LEVEL_ID')->dropDownList(
            ArrayHelper::map(PsUmnLevels::find()->all(),'UMN_LEVEL_ID','DESCR'),
            ['prompt'=>'Select Level']
    ) ?>

    <?= $form->field($model, 'DESCR_80')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DESCR50_1')->textInput([
                                'maxlength' => true,
                                'value'  => 'Universitas Multimedia Nusantara'
                            ]) ?>

    <?= $form->field($model, 'START_DATE')->widget(\yii\jui\DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
    'clientOptions' => ['defaultDate' => date('Y-m-d')],
    ]) ?>

    <?= $form->field($model, 'END_DATE')->widget(\yii\jui\DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
    // 'value'  => date('Y-m-d'),
    'clientOptions' => ['defaultDate' => date('Y-m-d')],
    ]) ?>

    <?= $form->field($model, 'COUNT_TOTAL')->textInput([
                                 'type' => 'number',
                                 'value'  => '1'
                            ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
