<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <meta content="Indonesia" name="geo.placename"/>
    <!-- <meta content="university, universitas, multimedia, nusantara, serpong, gading, tangerang, kompas, gramedia, online" name="KEYWORDS"/> -->
    <meta name="description" content="Universitas Multimedia Nusantara">
    <meta name="author" content="universitas multimedia nusantara">
    <!-- Icon-->
    <link rel="shortcut icon" href="<?php echo Url::toRoute('assets/QR/image/logo-umn.png', true)?>" type="image/png" >
    <!-- <link rel="shortcut icon" href="<?php //echo Yii::$app->request->baseUrl; ?>/assets/QR/image/Logo Student Dev UMN.png" type="image/png" > -->
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56c18d73f0b07e75" async="async"></script> -->
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
    <?php
    NavBar::begin([
        'brandLabel' => '<img src= "'.Url::toRoute('assets/QR/image/umnstudentdev.png', true).'" class="img-responsive" height="50" width="350">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-inverse navbar-fixed-top',
            'style' => 'min-height: 83px',
        ],
    ]);
    $menuItems = [
        // ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Scan QR Code', 'url' => Url::to(['/site/scan'])],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => Url::to(['/site/login'])];
    } else {
        $menuItems[] = ['label' => 'SKKM', 'url' => Url::to(['/skkm/index'])];   
        // $menuItems[] = ['label' => 'Student', 'url' => Url::to(['/skkm-student/index'])];       
        // $menuItems[] = ['label' => 'Create QR Code', 'url' => ['/site/create']];
        // $menuItems[] = ['label' => 'Create QR by Import Excel', 'url' => ['/site/import']];
        $menuItems[] = ['label' => 'Signup New Admin', 'url' => Url::to(['/site/signup'])];
        $menuItems[] = '<li>'
            . Html::beginForm(Url::to(['/site/logout']), 'post')
            . Html::submitButton(
                'Logout<br>(' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => [
            'class' => 'navbar-nav navbar-right center',
            'style' => 'margin-top:7px',
            ],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">

        <div class="col-md-8">
            <div class="col-md-2 text-center">
                <?= Html::img(Url::toRoute('assets/QR/image/Logo Student Dev UMN.png', true) , ['class' => 'img-responsive center','width' => '100px']); ?>
            </div>
            <div class="col-md-6" style = "color: #efefef">
                <p style="font-weight:bold">&copy; Universitas Multimedia Nusantara <?= date('Y') ?>.</p>
                <p class="small"><?= Yii::$app->params['adminPlace'];?><br/>
                Jl. Boulevard Gading Serpong, Tangerang, Banten, Indonesia<br/>
                (T)+62 21 5422 0808; (F)+62 21 5422 0800<br/>
                </p>
            </div>
        </div>
        <!-- <p class="pull-left">&copy; My Company <?= date('Y') ?></p> -->

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
