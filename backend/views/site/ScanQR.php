<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use backend\assets\AppQRAsset; 

AppQRAsset::register($this);
$this->title = 'Scan QR Code';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-scan">
    <h1><?= Html::encode($this->title) ?></h1>
	<div class="row center" id="mainbody">
		<div class="row center" style="display: none;">
			<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3"> </div>
			<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3"><img class="selector" id="webcamimg" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/QR/image/vid.png" onclick="setwebcam()" align="left"/></div>
			<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3"><img class="selector" id="qrimg" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/QR/image/cam.png" onclick="setimg()" align="right"/></div>
			<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3"> </div>
		</div>
		<div class="row center">
			<div class="col center" id="outdiv"></div>
		</div>
		<div class="row center">
			<div class="col center" id="result" style="display: none;"></div>
		</div>
	</div>
	<canvas id="qr-canvas" width="800" height="600"></canvas>
	<script type="text/javascript">load();</script>
     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?> 
</div>
