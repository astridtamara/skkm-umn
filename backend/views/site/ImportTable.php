<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
// use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\editable\Editable;
use kartik\widgets\Select2;
use backend\models\PsUmnParpntType;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\UMNSkkmEntry */
// $this->title = $model->SEQNUM_DAY1;
$this->title = 'Import Excel Peserta';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['event']=$event->SEQNUM_DAY1;
?>
<div class="site-create">
    <h1><?= Html::encode($this->title) . " '" . $event->DESCR_80  . "' " ;?> </h1>

     <?php $form = ActiveForm::begin(); ?>
    <?=
    // $this->context->ImportExcel();
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive'=>true,
        // 'filterModel' => $searchModel,
        // 'columns' => $gridColumns,
        'responsive'=>true,
        'hover'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'EMPLID',
            // 'UMN_PARTICIPANT_ID',
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'UMN_PARTICIPANT_ID',
                //mengambil nama participant untuk ditampilkan
                'value' => function($data){
                            if ($text = PsUmnParpntType::findOne(['UMN_PARTICIPANT_ID' => $data['UMN_PARTICIPANT_ID']])) {
                                return $text->DESCR;
                            } else {
                                return 'Pilih participant';
                            }
                        },   
                //menggunakan widget untuk editable column
                //list data type partisipant dropdown dari DB
                // 'editableOptions' => [
                
                //     'name' => 'PARTICIPANT',
                //     'header' => 'UMN_PARTICIPANT_ID',
                //     'format' => Editable::FORMAT_BUTTON,
                //     'asPopover' => false,
                //     'inputType' => '\kartik\select2\Select2',
                //     'options'=>
                //     [
                //         // 'placeholder' => 'Pilih tipe participant ...',
                //         'data' => ArrayHelper::map(PsUmnParpntType::find()->all(),'UMN_PARTICIPANT_ID', 'DESCR'),
                //     ],
                // ],
            ],
        ],
    ]);
    ?>
    <div class="form-group">
<?= Html::submitButton('Cancel', ['name' => 'Cancel', 'value' => 'Cancel', 'class' => 'btn btn-danger']) ?>
<?= Html::submitButton('Save', ['name' => 'Save', 'value' => 'Save', 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?> 

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
