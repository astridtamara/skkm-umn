<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
// use yii\data\ArrayDataProvider;
use yii\widgets\DetailView;
use backend\models\PsUmnParpntType;
// use frontend\assets\AppQRAsset; 
// use odaialali\qrcodereader\QrReader;
// use Zxing\QrReader;

// AppQRAsset::register($this);
$this->title = 'Scan QR Code';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-scan">
    <h1><?= Html::encode($this->title) ?></h1>
	<div class="row center" id="mainbody">
		<div class="row center">
		</div>
		<div class="row center">
			<div class="col center" id="outdiv"></div>
		</div>
		<div class="row center">
			<div class="col center" id="result">
    <?php 
        $encdata = $_GET['data'];
        $PKey = Yii::$app->session->get('PKey');
        // // $utf=utf8_encode($encdata);
        // $data = yii::$app->security->decryptByPassword(utf8_decode($encdata), $PKey);
        // $data = yii::$app->security->decryptByPassword(base64_decode($encdata), $PKey);
        
    if($ddata==false){
    	echo "can't decryp";
    }
    else if(is_array($ddata)){
    ?>
    	<?=
        DetailView::widget([
        'model' => $ddata,
        'attributes' => [
            // 'UMN_AREA_ID',
            // 'UMN_EVENT_ID',
            // 'UMN_LEVEL_ID',
            [
                'attribute' => 'EMPLID',
                'label' => 'NIM',
                // 'value' => 'EMPLID',
            ],
            [
                'attribute' => 'NM_MHS',
                'label' => 'Nama Mahasiswa',
                // 'value' => 'NM_MHS',
            ],
            [
                'attribute' => 'COUNT_TOTAL',
                'label' => 'Jumlah Absensi',
                // 'value' => 'COUNT_TOTAL',
            ],
            [
                'attribute' => 'Event',
                'label' => 'Nama Event',
                // 'value' => 'Event',
            ],
            [
                'attribute' => 'START_DATE',
                'label' => 'Tanggal Acara',
                // 'value' => 'START_DATE',
                'value' => function($data){
                            if ($data['START_DATE']==$data['END_DATE']) {
                                return $data['START_DATE'];
                            } else {
                                return $data['START_DATE'].' sampai '.$data['END_DATE'];
                            }
                        },   
            ],
            // [
            //     'attribute' => 'END_DATE',
            //     'label' => 'NIM',
            //     // 'value' => 'END_DATE',
            // ],
            [
                'attribute' => 'UMN_PARTICIPANT_ID',
                'label' => 'Tipe Partisipant',
                'value' => function($data){
                            if ($text = PsUmnParpntType::findOne(['UMN_PARTICIPANT_ID' => $data['UMN_PARTICIPANT_ID']])) {
                                return $text->DESCR;
                            } else {
                                return 'Pilih participant';
                            }
                        },   
            ],
            [
                'attribute' => 'Point',
                'label' => 'Jumlah Point',
                // 'value' => 'Event',
            ],
            [
                'attribute' => 'Area',
                'label' => 'Jenis Point SKKM',
                // 'value' => 'Area',
            ],
        ],
    ]);
    ?>
    <?php 
    }
    // echo $encdata."<br>";
    // echo $tes."<br>";
    // echo $utf."<br>";
    // echo $PKey;
?>
</div>
		</div>
	</div>
	<canvas id="qr-canvas" width="800" height="600"></canvas>
     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>