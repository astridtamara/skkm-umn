<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UMNStudentSKKM */

$this->title = 'Update UMN Student SKKM ';
$this->params['breadcrumbs'][] = ['label' => 'UMN Student SKKM', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uMNSkkmEntry->DESCR_80, 'url' => ['view', 'id' => $model->SEQNUM_DAY1]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="umnstudent-skkm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
