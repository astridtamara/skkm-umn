<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\PsUmnPntPerPct;
use kartik\icons\Icon;

// Initialize a specific framework - e.g. Web Hosting Hub Glyphs 
                            Icon::map($this, Icon::BSG);
/* @var $this yii\web\View */
/* @var $model backend\models\UMNStudentSKKM */
// $model->uMNSkkmEntry->DESCR_80
$this->title = 'Detail Point SKKM';
$this->params['breadcrumbs'][] = ['label' => 'UMN Student SKKM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="umnstudent-skkm-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'SEQNUM_DAY1' => $model->SEQNUM_DAY1,'EMPLID' => $model->EMPLID], ['class' => 'btn btn-primary']) ?>
        <!-- <= Html::a('Delete', ['delete', 'id' => $model->SEQNUM_DAY1], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?> -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'EMPLID',
            'mahasiswa.NM',
            'uMNSkkmEntry.DESCR_80',
            'uMNAREA.DESCR',
            'uMNEVENT.DESCR',
            'uMNLEVEL.DESCR',
            'INSTITUTION',
            'uMNPARTICIPANT.DESCR',
            'COUNT_TOTAL',
            // 'UMN_OBTAIN_POINTS',
            
            [
                'attribute' => 'ATTEND_PRESENT',
                'format' => 'html',
                'value' => function($data){
                            if ($data['ATTEND_PRESENT'] == 'Y') {
                                // return Icon::show('ok', ['aria-hidden'=>"true",'style'=>"color:green;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                                return 'Poin Sudah Masuk';
                            } else {
                                // return Icon::show('remove', ['aria-hidden'=>"true",'style'=>"color:red;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                                return'Poin Belum Masuk';
                            }
                        },
            ],
            // 'uMNEVENT.POINTS',
            [
                'attribute' => 'Point',
                'label' => 'Jumlah Point',
                'value' => function($data){
                            if ($text = PsUmnPntPerPct::find()->where(['UMN_AREA_ID' => $data['UMN_AREA_ID'],'UMN_EVENT_ID' => $data['UMN_EVENT_ID'],'UMN_LEVEL_ID' => $data['UMN_LEVEL_ID'],'UMN_PARTICIPANT_ID' => $data['UMN_PARTICIPANT_ID']])->one()) {
                                return $text->POINTS;
                            } else {
                                return 'Pilih participant';
                            }
                        },   
            ],
        ],
    ]) ?>

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
