<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
        use yii\helpers\Url;
use backend\models\PsUmnAreas;
use backend\models\PsUmnEvents;
use backend\models\PsUmnLevels;
use backend\models\PsUmnSkkmEntry;
use backend\models\PsUmnStdntVw;
use backend\models\PsUmnParpntType;
// use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\UMNStudentSKKM */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="umnstudent-skkm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'SEQNUM_DAY1')->dropDownList(
            ArrayHelper::map(PsUmnSkkmEntry::find()->orderBy(['DESCR_80'=>SORT_ASC])->all(),'SEQNUM_DAY1','DESCR_80'),
            ['class'=>'form-control event','prompt'=>'Select Event','disabled' =>  $model->isNewRecord ? false : true]
    ) ?>


    <?= $form->field($model, 'EMPLID')->textInput(['type' => 'number','maxlength' => true,'disabled' => $model->isNewRecord ? false : true])?> <!-- 'readonly' => true]) ?> -->
     
    <?= $form->field($model,'Nama')->textInput(['type' => $model->isNewRecord ? 'hidden':'text', 'disabled' => true, 'value' => $model->isNewRecord ? 'Nama Mahasiswa' : ($model->mahasiswa->NM)])->label($model->isNewRecord ? false : 'Nama Mahasiswa')?> <!-- 'readonly' => true]) ?> -->
    <!-- <= $form->field($model, 'EMPLID')->dropDownList(
            ArrayHelper::map(PsUmnStdntVw::find()->orderBy(['EMPLID'=>SORT_ASC])->all(),'EMPLID','EMPLID'),
            ['prompt'=>'Select Event']
    ) ?> -->

    <!-- <= $form->field($model, 'UMN_PARTICIPANT_ID')->textInput(['type' => 'number']) ?> 

$this->registerJs('
 $(".event").change(function(){
    var value = this.value;
    if(value == "MALE"){
    $(".participant").val("0");
    }
    if(value == "FEMALE"){
    $(".participant").val("4");
    }
    if(value == "UNSPECIFIED"){
    $(".participant").val("23");
 }
});
    -->
    <?= $form->field($model, 'UMN_PARTICIPANT_ID')->dropDownList(
            ArrayHelper::map(PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where($model->isNewRecord ?  : ['UMN_AREA_ID' => $model->UMN_AREA_ID,'UMN_EVENT_ID' => $model->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $model->UMN_LEVEL_ID])->all(),'UMN_PARTICIPANT_ID','DESCR'),
            ['class'=>'form-control participant','prompt'=>'Select Participant']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
