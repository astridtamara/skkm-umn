<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\UMNStudentSKKM */

$this->title = 'Create UMN Student SKKM';
$this->params['breadcrumbs'][] = ['label' => 'UMN Student SKKM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="umnstudent-skkm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
