<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use kartik\widgets\Select2;
use backend\models\PsUmnParpntType;
use backend\models\PsUmnPntPerPct;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
// Initialize a specific framework - e.g. Web Hosting Hub Glyphs 
                            Icon::map($this, Icon::BSG);
/* @var $this yii\web\View */
/* @var $searchModel backend\models\SkkmStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'UMN Student SKKM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="umnstudent-skkm-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create UMN Student SKKM', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'INSTITUTION',
            'EMPLID',
            [
                'attribute' => 'Nama',
                'label' => 'Nama Mahasiswa',
                'value' => 'mahasiswa.NM',
            ],
            // 'uMNAREA.DESCR',
            // 'uMNEVENT.DESCR',
            // 'uMNLEVEL.DESCR',
            // 'uMNSkkmEntry.DESCR_80',
            // 'uMNPARTICIPANT.DESCR',
            // 'COUNT_TOTAL',
            // 'UMN_OBTAIN_POINTS',
            // 'ATTEND_PRESENT',
            // 'uMNEVENT.POINTS',
            [
                // 'label' => 'Events',
                'attribute' => 'SEQNUM_DAY1',
                'value' => 'uMNSkkmEntry.DESCR_80',
            ],
            [
                'attribute' => 'UMN_PARTICIPANT_ID',
                'value' => 'uMNPARTICIPANT.DESCR',
                        //menggunakan widget untuk editable column
                        'class' => 'kartik\grid\EditableColumn',
                        //list data dropdown dari DB
                        'editableOptions' => [
                            'header' => 'tipe partisipan',
                            'asPopover' => false,
                            'format' => Editable::FORMAT_BUTTON,
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'inputType' => '\kartik\select2\Select2',
                            'options'=>
                            [
                                'data' => ArrayHelper::map(PsUmnParpntType::find()->all(),'UMN_PARTICIPANT_ID', 'DESCR'),
                            ],
                        ],
            ],
            [
                'label' => 'Points',
                'attribute' => 'Points',
                'value' => function($data){
                    if ($text = PsUmnPntPerPct::findOne(['UMN_AREA_ID' => $data['UMN_AREA_ID'],'UMN_EVENT_ID' => $data['UMN_EVENT_ID'],'UMN_LEVEL_ID' => $data['UMN_LEVEL_ID'],'UMN_PARTICIPANT_ID' => $data['UMN_PARTICIPANT_ID']])->POINTS) {
                        return $text;
                    }
                    else return 0;
                        },   
            ],
            [
                'label' => 'Status',
                'attribute' => 'ATTEND_PRESENT',
                'format' => 'html',
                'value' => function($data){
                            if ($data['ATTEND_PRESENT'] == 'Y') {
                                return Icon::show('ok', ['aria-hidden'=>"true",'style'=>"color:green;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                            } else {
                                return Icon::show('remove', ['aria-hidden'=>"true",'style'=>"color:red;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                            }
                        },   
                
            ],
            // 'ATTEND_PRESENT',
            [   
                'class' => 'yii\grid\ActionColumn', 
                'template' => '{view} {update}',
                'controller' => 'skkm-student'     
            ],
        ],
    ]); ?>
    <?php if (Yii::$app->session->hasFlash('Error')): ?>
    <div class="alert alert-danger alert-dismissable">
        <!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
        <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
        <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
    </div>
    <?php endif; ?>
</div>
