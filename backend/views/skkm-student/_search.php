<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SkkmStudentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="umnstudent-skkm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'INSTITUTION') ?>

    <?= $form->field($model, 'UMN_AREA_ID') ?>

    <?= $form->field($model, 'UMN_EVENT_ID') ?>

    <?= $form->field($model, 'UMN_LEVEL_ID') ?>

    <?= $form->field($model, 'SEQNUM_DAY1') ?>

    <?php // echo $form->field($model, 'EMPLID') ?>

    <?php // echo $form->field($model, 'UMN_PARTICIPANT_ID') ?>

    <?php // echo $form->field($model, 'COUNT_TOTAL') ?>

    <?php // echo $form->field($model, 'UMN_OBTAIN_POINTS') ?>

    <?php // echo $form->field($model, 'ATTEND_PRESENT') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
