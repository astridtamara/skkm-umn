<?php

namespace backend\controllers;

use Yii;
use backend\models\PsUmnStdntAcvty;
use backend\models\SkkmStudentSearch;
use backend\models\PsUmnParpntType;
use backend\models\PsUmnSkkmEntry;
use backend\models\PsUmnStdntVw;
use backend\models\PsUmnPntPerPct;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

/**
 * SkkmStudentController implements the CRUD actions for UMNStudentSKKM model.
 */
class SkkmStudentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'=> [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],//must login to access
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@']// hanya dappat diakses oleh user setelah login
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UMNStudentSKKM models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SkkmStudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(Yii::$app->request->post('hasEditable'))
        {
            $studentEdit = $_POST['PsUmnStdntAcvty'];
            $row = $_POST['editableIndex'];
                $rowDetail= Yii::$app->request->post('editableKey');
                // var_dump($row);
                $find = array(":","{","}",",","\"");
                $rowDetail=str_replace($find, ' ', $rowDetail);

                $editableKey[] = array_filter(explode(' ', $rowDetail));
                $edit[$editableKey[0][2]]=$editableKey[0][4];//SEQNUM_DAY1
                $edit[$editableKey[0][6]]=$editableKey[0][9];//EMPLID
                $edit['UMN_PARTICIPANT_ID'] = $studentEdit[$row]['UMN_PARTICIPANT_ID'];
                $edit['PARTICIPANT_TYPE'] = PsUmnParpntType::findOne(['UMN_PARTICIPANT_ID' => $edit['UMN_PARTICIPANT_ID']])->DESCR;
            
            $StdntAcvty = $this->findModel($edit['SEQNUM_DAY1'],$edit['EMPLID']);

            $out =Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['PsUmnStdntAcvty']);
            $post['PsUmnStdntAcvty'] = $posted;

            $Point = PsUmnPntPerPct::find()
            ->where(['UMN_AREA_ID' => $StdntAcvty->UMN_AREA_ID,'UMN_EVENT_ID' => $StdntAcvty->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $StdntAcvty->UMN_LEVEL_ID, 'UMN_PARTICIPANT_ID' => $edit->UMN_PARTICIPANT_ID])
            ->one();
            if ($Point== null) {
                Yii::$app->session->setFlash('error','Point SKKM untuk event dan type participant tersebut tidak ditemukan. ');
                $ddata=$EMPLID;
                $output = 'Point SKKM tidak ditemukan. ';
                $message = $edit['PARTICIPANT_TYPE'];
                $out = Json::encode(['output'=> $message,'message'=>$output]);
            }        
            else if($StdntAcvty->load($post))
            {
                // // save data partisipant yang di update
                $StdntAcvty->save();
                $output = 'NIM : ' . $edit['EMPLID'] . ' - PARTICIPANT ID : ' . $edit['UMN_PARTICIPANT_ID'];
                $message = $edit['PARTICIPANT_TYPE'];
                $out = Json::encode(['output'=> $message,'message'=>'']);
            }
            echo $out;
            return;
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UMNStudentSKKM model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($SEQNUM_DAY1,$EMPLID)
    {
        return $this->render('view', [
            'model' => $this->findModel($SEQNUM_DAY1,$EMPLID),
        ]);
    }

    /**
     * Creates a new UMNStudentSKKM model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PsUmnStdntAcvty();

        if ($model->load(Yii::$app->request->post())) {
            $Event = PsUmnSkkmEntry::findOne($model->SEQNUM_DAY1);
            $model->INSTITUTION=$Event->INSTITUTION;
            $model->UMN_AREA_ID=$Event->UMN_AREA_ID;
            $model->UMN_EVENT_ID=$Event->UMN_EVENT_ID;
            $model->UMN_LEVEL_ID=$Event->UMN_LEVEL_ID;
            $Mahasiswa = PsUmnStdntVw::find()
            ->where('EMPLID = :EMPLID', [':EMPLID' => $model->EMPLID])
            ->one();

            $Point = PsUmnPntPerPct::find()
            ->where(['UMN_AREA_ID' => $model->UMN_AREA_ID,'UMN_EVENT_ID' => $model->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $model->UMN_LEVEL_ID, 'UMN_PARTICIPANT_ID' => $model->UMN_PARTICIPANT_ID])
            ->one();

            if($Mahasiswa == null)            
            {
                Yii::$app->session->setFlash('error','Mahasiswa dengan NIM ' . $EMPLID . ' tidak ditemukan. ');
                $ddata=$EMPLID;
            }
            else if($Event == null)
            {
                Yii::$app->session->setFlash('error','Event tidak ditemukan. Event = ' . $SEQNUM_DAY1 .'. NIM mahasiswa = ' . $EMPLID);
                $ddata=$EMPLID;
            }
            else if ($Point== null) {
                Yii::$app->session->setFlash('error','Point SKKM untuk event dan type participant tersebut tidak ditemukan. ');
                $ddata=$EMPLID;
            } 
            else if ($model->save()) {
            return $this->redirect(['view', 'SEQNUM_DAY1' => $model->SEQNUM_DAY1, 'EMPLID' => $model->EMPLID]);
             }

        } 
            return $this->render('create', [
                'model' => $model,
            ]);
    }

    /**
     * Updates an existing UMNStudentSKKM model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($SEQNUM_DAY1,$EMPLID)
    {
        $model = $this->findModel($SEQNUM_DAY1,$EMPLID);

        if ($model->load(Yii::$app->request->post())) {
            $Mahasiswa = PsUmnStdntVw::find()
            ->where('EMPLID = :EMPLID', [':EMPLID' => $model->EMPLID])
            ->one();
            $Point = PsUmnPntPerPct::find()
            ->where(['UMN_AREA_ID' => $model->UMN_AREA_ID,'UMN_EVENT_ID' => $model->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $model->UMN_LEVEL_ID, 'UMN_PARTICIPANT_ID' => $model->UMN_PARTICIPANT_ID])
            ->one();
            if($Mahasiswa == null)            
            {
                Yii::$app->session->setFlash('error','Mahasiswa dengan NIM ' . $EMPLID . ' tidak ditemukan. ');
                $ddata=$EMPLID;
            }
            else if ($Point== null) {
                Yii::$app->session->setFlash('error','Point SKKM untuk event dan type participant tersebut tidak ditemukan. ');
                $ddata=$EMPLID;
            }            
            else if ($model->save()) {
                return $this->redirect(['view', 'SEQNUM_DAY1' => $model->SEQNUM_DAY1, 'EMPLID' => $model->EMPLID]);
            }
        }
            return $this->render('update', [
                'model' => $model,
            ]);
    }

    /**
     * Deletes an existing UMNStudentSKKM model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
    public function actionDelete($SEQNUM_DAY1,$EMPLID)
    {
        $this->findModel($SEQNUM_DAY1,$EMPLID)->delete();

        return $this->redirect(['index']);
    }
     */

    /**
     * Finds the UMNStudentSKKM model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UMNStudentSKKM the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($SEQNUM_DAY1,$EMPLID)
    {
        if (($model = PsUmnStdntAcvty::findOne(['SEQNUM_DAY1' => $SEQNUM_DAY1,'EMPLID' => $EMPLID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
