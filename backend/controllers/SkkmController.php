<?php

namespace backend\controllers;

use Yii;
use ZipArchive;
use common\models\User;
use common\models\ImportForm;
use common\widgets\QRCodeGenerator;
use yii\base\Security;
use yii\base\Exception;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use backend\models\Panitia;
use backend\models\PsUmnEvents;
use backend\models\PsUmnParpntType;
use backend\models\PsUmnPntPerPct;
use backend\models\PsUmnStdntVw;
use backend\models\PsUmnSkkmEntry;
use backend\models\PsUmnStdntAcvty;
use backend\models\SkkmSearch;
use backend\models\SkkmStudentSearch;

/**
 * SkkmController implements the CRUD actions for UMNSkkmEntry model.
 */
class SkkmController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'=> [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'lists', 'import', 'importview'],//must login to access
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'lists', 'import', 'importview'],
                        'allow' => true,
                        'roles' => ['@']// hanya dappat diakses oleh user setelah login
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UMNSkkmEntry models.
     * @return mixed
     *
     * menampilkan halaman yang akan diakses pertama kali saat membuka web
     */
    public function actionIndex()
    {
        $searchModel = new SkkmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UMNSkkmEntry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $skkm = $this->findModel($id);
        $panitia = Panitia::find()->where(['SEQNUM_DAY1' => $skkm->SEQNUM_DAY1])->one();
        if ($panitia) {
        $user = User::find()->where(['id' => $panitia->UserID])->one();
        $PKey = $panitia->PKey;
        $username = $user->username;
        }
        else{        
            $PKey=0;
            $username='Admin';    
        }

        // tabel student actifity
        $searchModel = new SkkmStudentSearch();
        $searchModel->SEQNUM_DAY1=$skkm->SEQNUM_DAY1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->request->post('hasEditable'))
        {
            $studentEdit = $_POST['PsUmnStdntAcvty'];
            $row = $_POST['editableIndex'];

            $rowDetail= Yii::$app->request->post('editableKey');
            $find = array(":","{","}",",","\"");
            $rowDetail=str_replace($find, ' ', $rowDetail);

            $editableKey[] = array_filter(explode(' ', $rowDetail));
            $edit[$editableKey[0][2]]=$editableKey[0][4];//SEQNUM_DAY1
            $edit[$editableKey[0][6]]=$editableKey[0][9];//EMPLID
            $edit['UMN_PARTICIPANT_ID'] = $studentEdit[$row]['UMN_PARTICIPANT_ID'];
            $edit['PARTICIPANT_TYPE'] = PsUmnParpntType::findOne(['UMN_PARTICIPANT_ID' => $edit['UMN_PARTICIPANT_ID']])->DESCR;
            
            $StdntAcvty = $this->findModel($edit['SEQNUM_DAY1'],$edit['EMPLID']);

            $out =Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['PsUmnStdntAcvty']);
            $post['PsUmnStdntAcvty'] = $posted;

            $Point = PsUmnPntPerPct::find()
            ->where(['UMN_AREA_ID' => $StdntAcvty->UMN_AREA_ID,'UMN_EVENT_ID' => $StdntAcvty->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $StdntAcvty->UMN_LEVEL_ID, 'UMN_PARTICIPANT_ID' => $edit->UMN_PARTICIPANT_ID])
            ->one();
            if ($Point== null) {
                Yii::$app->session->setFlash('error','Point SKKM untuk event dan type participant tersebut tidak ditemukan. ');
                $ddata=$EMPLID;
                $output = 'Point SKKM tidak ditemukan. ';
                $message = $edit['PARTICIPANT_TYPE'];
                $out = Json::encode(['output'=> $message,'message'=>$output]);
            }        
            else if($StdntAcvty->load($post))
            {
                // save data partisipant yang di update
                $StdntAcvty->save();
                $output = 'NIM : ' . $edit['EMPLID'] . ' - PARTICIPANT ID : ' . $edit['UMN_PARTICIPANT_ID'];
                $message = $edit['PARTICIPANT_TYPE'];
                $out = Json::encode(['output'=> $message,'message'=>'']);
            }
            echo $out;
            return;
        }

        return $this->render('view', 
            [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($id),
            'PKey' => $PKey,
            'username' => $username,
            ]
        );
    }


    /**
     * Creates a new UMNSkkmEntry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = $this->findModel($id);
        $panitia = new Panitia();
        $panitia->SEQNUM_DAY1 = $model->SEQNUM_DAY1;
        $panitia->PKey = Yii::$app->security->generateRandomString(6);
        $username = $model->INSTITUTION . date("Y") . "-" . $model->SEQNUM_DAY1;
        $panitia->signup($username);
        $panitia->save();
        return $this->redirect(['view', 'id' => $model->SEQNUM_DAY1]);
    }

    /**
     * Updates an existing UMNSkkmEntry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     *
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->SEQNUM_DAY1]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UMNSkkmEntry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     *
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

     */

    /**
     * Finds the UMNSkkmEntry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UMNSkkmEntry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PsUmnSkkmEntry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * mengambil data list events berdasarkan area
     */
    public function actionLists($id)
    {
        $rows = PsUmnEvents::find()->where(['UMN_AREA_ID' => $id])->all();
 
        echo "<option>Select Event</option>";
 
        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->UMN_EVENT_ID'>$row->DESCR</option>";
            }
        }
        else{
            echo "<option>There is no event</option>";
        }
 
    }
    /**
     * menampilkan halaman Import excel untuk membuat QR Code
     */
    public function actionImport($id)
    {
        $userid = $id;
        $SKKM = PsUmnSkkmEntry::find()
            ->where('SEQNUM_DAY1 = :id', [':id' => $id])
            ->one();
        $Panitia = Panitia::find()
            ->where('SEQNUM_DAY1 = :userid', [':userid' => $id])
            ->one();
        Yii::$app->session->set('PKey', $Panitia->PKey);

        $model = new ImportForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->excelFile = UploadedFile::getInstance($model, 'excelFile');
            if ($model->upload()) {
            return $this->redirect(['importview', 
                'event'  => $id,
                'excelFile' => $model->excelFile->name,
                'errorCorrectionLevel' => 'L', 
                'matrixPointSize'=> 7
                ]);
            }
        }
        return $this->render('Import', [
                'model' => $model,
                'event' => $SKKM,
            ]);
    }

    /**
     * menampilkan halaman hasil create QR Code dari Import excel
     */
    public function actionImportview($event,$excelFile,$errorCorrectionLevel,$matrixPointSize)
    {
        $PKey = Yii::$app->session->get('PKey');
        $inputFile = 'uploads/' . $excelFile;
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFile);
        } catch (Exception $e) {
            die('Error');
        }
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row=1; $row <= $highestRow; $row++) { 
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null,true,false);
            if($row==1 || $rowData[0][0] == '')
            {
                continue;
            }
            $string = (string)$rowData[0][0] ;// ambil cel collumn pertama 

            if((PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $event,'EMPLID' => $rowData[0][0]])->one() == null)&&(PsUmnStdntVw::find()->where(['EMPLID' => $rowData[0][0]])->one()))            
            {
                $SkkmEvent = PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $event])->one();
                    $student = new PsUmnStdntAcvty();
                    $student->UMN_AREA_ID = $SkkmEvent->UMN_AREA_ID;
                    $student->UMN_EVENT_ID = $SkkmEvent->UMN_EVENT_ID;
                    $student->UMN_LEVEL_ID = $SkkmEvent->UMN_LEVEL_ID;
                    $student->INSTITUTION = $SkkmEvent->INSTITUTION;
                    $student->SEQNUM_DAY1 = $event;
                    $student->EMPLID = $string; // ambil cel collumn pertama sebagai nim

                $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID,'DESCR' => $rowData[0][1]])->one();
                if($PARTICIPANT != null){
                        $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else if ($PARTICIPANT->UMN_PARTICIPANT_ID == null) {
                    $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID])->one();
                    $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else {
                    $student->UMN_PARTICIPANT_ID = 4; 
                }
                if(!$student->validate()){
                    print_r($student);
                    throw new Exception("Student " .$student->EMPLID." could not be validated ");
                } 
                if(!$student->save()) throw new Exception("Student " .$string. " could not be saved ");
                
            }

            if(PsUmnStdntVw::find()->where(['EMPLID' => $string])->one()){
            $encdata = Yii::$app->security->encryptByPassword($string.'#'.$event, $PKey);
            $b64=base64_encode($encdata);
            $b64 = str_replace(array('+','/','='),array('-','_',' '),$b64);
                QRCodeGenerator::widget([
                            'data' => $PKey.$b64,
                            'filename' => $rowData[0][0] . ".png",
                            'subfolderVar' => $event,
                            'displayImage'=>true,
                            'errorCorrectionLevel'=> $errorCorrectionLevel,
                            'matrixPointSize'=> $matrixPointSize, // 1 to 10 only
                            ]);
            $students[] = array('nim'=>(string)$rowData[0][0]);
             }
            else{
                $students[] = array('nim'=> (string)$rowData[0][0]);
            }
        }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $students,
                'sort' => [
                    'attributes' => ['nim'],
                ],
            ]);
        $SKKM = PsUmnSkkmEntry::find()
            ->where('SEQNUM_DAY1 = :event', [':event' => $event])
            ->one();
        return $this->render('ImportTable', [
                'dataProvider' => $dataProvider,
                'event' => $SKKM,
            ]);
    }

    /**
     * memasukan seluruh file image QR yang telah dibuat kedalam zip
     */
    private static function folderToZip($folder, &$zipFile, $exclusiveLength) 
    { 
        $handle = opendir($folder); 
        while (false !== $f = readdir($handle)) { 
          if ($f != '.' && $f != '..') { 
            $filePath = "$folder/$f"; 
            // Remove prefix from file path before add to zip. 
            $localPath = substr($filePath, $exclusiveLength); 
            if (is_file($filePath)) { 
              $zipFile->addFile($filePath, $localPath); 
            } elseif (is_dir($filePath)) { 
              // Add sub-directory. 
              $zipFile->addEmptyDir($localPath); 
              self::folderToZip($filePath, $zipFile, $exclusiveLength); 
            } 
          } 
        } 
        closedir($handle); 
    } 
    /**
     * mempersiapkan data yang akan di zip
     */
    public static function zipDir($sourcePath, $outZipPath) 
    { 
        $pathInfo = pathInfo($sourcePath); 
        $parentPath = $pathInfo['dirname']; 
        $dirName = $pathInfo['basename']; 

        $z = new ZipArchive(); 
        $z->open($outZipPath, ZIPARCHIVE::CREATE); 
        $z->addEmptyDir($dirName); 
        self::folderToZip($sourcePath, $z, strlen("$parentPath/")); 
        $z->close(); 
    } 

    /**
     * mendownload seluruh file image QR yang telah dibuat
     */
    public function actionDownload($id)
    {
        // untuk delete semua file zip
        // foreach(glob(realpath(Yii::$app->basePath.'/web/assets/QR/temp/').'/*.zip') as $file){
        //  unlink($file);
        // }
      $dir=realpath(Yii::$app->basePath.'/web/assets/QR/temp/').'/'.$id;

      if(file_exists($dir))
        {
        self::zipDir($dir, $dir.'.zip');
        
            $filesdel = array_diff(scandir($dir), array('.','..')); 
            foreach ($filesdel as $file) { 
              (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
            } 
            rmdir($dir);
          }
      if(file_exists($dir.'.zip'))
        {
          Yii::$app->response->sendFile($dir.'.zip');
        }
      else{throw new CHttpException(404, 'The requested file does not exist.');}

        // unlink($dir.'.zip');//untuk delete zip
    }

}
