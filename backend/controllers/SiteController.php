<?php
namespace backend\controllers;

use Yii;
// use yii\base\InvalidParamException;
use yii\base\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\data\ArrayDataProvider;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ContactForm;
use common\models\LoginForm;
use common\models\CreateForm;
use common\models\ImportForm;
use common\models\ImportPesertaForm;
use common\models\User;
use common\widgets\QRCodeGenerator;
use backend\models\PsUmnSkkmEntry;
use backend\models\Panitia;
use backend\models\SignupForm;
use backend\models\SkkmStudentSearch;
use backend\models\PsUmnStdntAcvty;
use backend\models\PsUmnParpntType;
use backend\models\PsUmnStdntVw;
use backend\models\PsUmnPntPerPct;
use backend\models\PsUmnAreas;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'scan', 'scanresult'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'signup', 'scan', 'create', 'import', 'importview','save'],
                        'allow' => true,
                        'roles' => ['@'],// hanya dappat diakses oleh user setelah login
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * menampilkan halaman yang akan diakses pertama kali saat membuka web
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('ScanQR');
    }
    /**
     * menampilkan halaman scan QR
     */
    public function actionScan()
    {
        return $this->render('ScanQR');
    }
    /**
     * memproses data scan qr dan menampilkan hasilnya
     */
    public function actionScanresult()
    {
        $qrdata = $_GET['data'];
        $b64 = substr($qrdata,6);
        $PKey = substr($qrdata,0,6);
        $b64 = str_replace(array('-','_',' '),array('+','/','='),$b64);
            $encdata=base64_decode($b64);

        $dat = yii::$app->security->decryptByPassword($encdata, $PKey);

        // try {
            $data[] = array_filter(explode('#', $dat));
            // $EMPLID = $data[0][0];
            // $SEQNUM_DAY1 = $data[0][1];
            $EMPLID = isset($data[0][0]) ? $data[0][0] : null;
            $SEQNUM_DAY1 = isset($data[0][1]) ? $data[0][1] : null;
        // } catch (Exception $e) {
        //     Yii::$app->session->setFlash('error','ERROR Scan QR. Silahkan coba lagi. ');
        // }
        if($EMPLID!=null||$SEQNUM_DAY1!=null){

            $Panitia = Panitia::find()
                ->where('PKey = :PKey AND SEQNUM_DAY1 = :SEQNUM_DAY1', [':PKey' => $PKey, 'SEQNUM_DAY1' => $SEQNUM_DAY1])
                ->one();
            $Event = PsUmnSkkmEntry::find()
                ->where('SEQNUM_DAY1 = :SEQNUM_DAY1', [':SEQNUM_DAY1' => $SEQNUM_DAY1])
                ->one();
            $StdntAcvty = PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $SEQNUM_DAY1,'EMPLID' => $EMPLID])->one();
            $UMNArea = PsUmnAreas::find()->where(['UMN_AREA_ID' => $Event->UMN_AREA_ID])->one();
            $Mahasiswa = PsUmnStdntVw::find()
                ->where('EMPLID = :EMPLID', [':EMPLID' => $EMPLID])
                ->one();
        if($qrdata == 'can\'t decryp')            
        {
            Yii::$app->session->setFlash('error','ERROR Scan. Silahkan coba lagi. ');
            $ddata=$EMPLID;
        }
        else if($Mahasiswa == null)            
        {
            Yii::$app->session->setFlash('error','Mahasiswa dengan NIM ' . $EMPLID . ' tidak ditemukan. ');
            $ddata=$EMPLID;
        }
        else if($Event == null)
        {
            Yii::$app->session->setFlash('error','Event tidak ditemukan. Event = ' . $SEQNUM_DAY1 .'. NIM mahasiswa = ' . $EMPLID);
            $ddata=$EMPLID;
        }
        else if($StdntAcvty != null)            
        {
            if ($StdntAcvty->COUNT_TOTAL >=  $Event->COUNT_TOTAL) {
                $StdntAcvty->ATTEND_PRESENT = 'Y';
                $StdntAcvty->save();

                $SKKM = PsUmnPntPerPct::find()->where(['UMN_AREA_ID' => $Event->UMN_AREA_ID,'UMN_EVENT_ID' => $Event->UMN_EVENT_ID,'UMN_LEVEL_ID' => $Event->UMN_LEVEL_ID,'UMN_PARTICIPANT_ID' => $StdntAcvty->UMN_PARTICIPANT_ID])->one();

                $ddata = [];
                $ddata['Event'] = $Event->DESCR_80;
                $ddata['START_DATE'] = $Event->START_DATE;
                $ddata['END_DATE'] = $Event->END_DATE;
                $ddata['UMN_PARTICIPANT_ID'] = $StdntAcvty->UMN_PARTICIPANT_ID;
                $ddata['COUNT_TOTAL'] = $StdntAcvty->COUNT_TOTAL ;
                $ddata['EMPLID'] = $Mahasiswa->EMPLID;
                $ddata['NM_MHS'] = $Mahasiswa->NM;
                $ddata['Point'] = $SKKM->POINTS;
                $ddata['Area'] = $UMNArea->DESCRFORMAL;
                Yii::$app->session->setFlash('success','SKKM point sudah masuk. ');
            }
            else{
                $ddata=$EMPLID;
                Yii::$app->session->setFlash('error','Absensi kurang. NIM mahasiswa = ' . $EMPLID);
            }

        }
        else{
            Yii::$app->session->setFlash('error','SKKM belum terdaftar. NIM mahasiswa = ' . $EMPLID);
            $ddata=$EMPLID;
        }
        }
        else{
            Yii::$app->session->setFlash('error','ERROR Scan QR. Silahkan coba lagi. ');
            $ddata=$dat;
        }
        return $this->render('ScanQRresult', [
                'ddata' => $ddata,
            ]);
    }
    /**
     * menampilkan halaman Create QR
     */
    public function actionCreate($id)
    {
        if (Yii::$app->user->isGuest) {
        
        $model = new LoginForm();
            return $this->redirect(['login']);
        }
        $Panitia = Panitia::find()
            ->where('SEQNUM_DAY1 = :id', [':id' => $id])
            ->one();
        $Event = PsUmnSkkmEntry::find()
            ->where('SEQNUM_DAY1 = :SEQNUM_DAY1', [':SEQNUM_DAY1' => $id])
            ->one();
        Yii::$app->session->set('PKey', $Panitia->PKey);

        $model = new CreateForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if((PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $Panitia->SEQNUM_DAY1,'EMPLID' => $model->nim])->one() == null) && (PsUmnStdntVw::find()->where(['EMPLID' => $model->nim])->one()))            
            {
                $SkkmEvent = PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $Panitia->SEQNUM_DAY1])->one();
                    $student = new PsUmnStdntAcvty();
                    $student->UMN_AREA_ID = $SkkmEvent->UMN_AREA_ID;
                    $student->UMN_EVENT_ID = $SkkmEvent->UMN_EVENT_ID;
                    $student->UMN_LEVEL_ID = $SkkmEvent->UMN_LEVEL_ID;
                    $student->INSTITUTION = $SkkmEvent->INSTITUTION;
                    $student->SEQNUM_DAY1 = $Panitia->SEQNUM_DAY1;
                    $student->EMPLID = $model->nim; // ambil cel collumn pertama sebagai nim

                $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID])->one();
                if($PARTICIPANT != null){
                        $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else if ($PARTICIPANT->UMN_PARTICIPANT_ID == null) {
                    $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID])->one();
                    if($PARTICIPANT != null){
                        $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                    }
                    else{
                        $student->UMN_PARTICIPANT_ID = 4; 
                    }
                }
                if(!$student->validate()) throw new Exception("Student " .$student->EMPLID." could not be validated ");
                if(!$student->save()) throw new Exception("Student " .$model->nim. " could not be saved ");
                
            }
            if(PsUmnStdntVw::find()->where(['EMPLID' => $model->nim])->one()){
            $encdata = Yii::$app->security->encryptByPassword($model->nim.'#'.$Panitia->SEQNUM_DAY1, $Panitia->PKey);
            $b64=base64_encode($encdata);
            $b64 = str_replace(array('+','/','='),array('-','_',' '),$b64);
                QRCodeGenerator::widget([
                            'data' => $Panitia->PKey.$b64,
                            'filename' => $model->nim . ".png",
                            'subfolderVar' => false,
                            'displayImage'=>true,
                            'errorCorrectionLevel' => 'L', 
                            'matrixPointSize'=> 7// 1 to 10 only
                            ]);
                Yii::$app->session->setFlash('success','QRCode Created');
            }
            else{
                Yii::$app->session->setFlash('error', 'Mahasiswa tidak ditemukan'); 
            }
        } else {
                // Yii::$app->session->setFlash('error', 'There was an error.');            
        }
        return $this->render('CreateQR', [
                'model' => $model,
                'event' => $Event,
            ]);
    }

    /**
     * menampilkan Import peserta
     */
    public function actionImport($id)
    {
        if (Yii::$app->user->isGuest) {
        
        $model = new LoginForm();
            return $this->redirect(['login']);
        }

        $model = new ImportPesertaForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->excelFile = UploadedFile::getInstance($model, 'excelFile');//mengupload file excel
            if ($model->upload()) {
            return $this->redirect(['importview', 
                'event'  => $id,
                'excelFile' => $model->excelFile->name
                ]);
            }
            else {
                Yii::$app->session->setFlash('Error','Upload file gagal');
            }
        }
        $SKKM = PsUmnSkkmEntry::find()
            ->where('SEQNUM_DAY1 = :id', [':id' => $id])
            ->one();
        return $this->render('Import', [
                'model' => $model,
                'event' => $SKKM,
            ]);
    }

    /**
     * memproses data Import peserta dan menampilkan hasilnya
     */
    public function actionImportview($event,$excelFile)
    {
        if (Yii::$app->user->isGuest) {
        
        $model = new LoginForm();
            return $this->redirect(['login']);
        }

        $searchModel = new SkkmStudentSearch();
        $SkkmEvent = PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $event])->one();

        $inputFile = 'uploads/' . $excelFile; // ambil alamat file excel yg d upload

        // baca file excel
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFile);
        } catch (Exception $e) {
                Yii::$app->session->setFlash('Error','File tidak terbaca');
        }
        $sheet = $objPHPExcel->getSheet(0);//baca sheet pertama
        $highestRow = $sheet->getHighestRow();//mengambil row teratas
        $highestColumn = $sheet->getHighestColumn();//mengambil kolom paling kiri

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();//memulai transaction supaya jika tidak jadi input dapat di rollback data yang telah diinputkan

        try {

            for ($row=1; $row <= $highestRow; $row++) { 
                $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null,true,false);

                // jika row adalah judul atau row tsb kosong lewati
                if($row==1 || $rowData[0][0] == '')
                {
                    continue;
                }

                // jika data sudah ada d db lewati
                if(PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $event,'EMPLID' => $rowData[0][0]])->one() != null)            
                {
                    continue;
                }

                $student = new PsUmnStdntAcvty();
                $student->UMN_AREA_ID = $SkkmEvent->UMN_AREA_ID;
                $student->UMN_EVENT_ID = $SkkmEvent->UMN_EVENT_ID;
                $student->UMN_LEVEL_ID = $SkkmEvent->UMN_LEVEL_ID;
                $student->INSTITUTION = 'UMNKG';
                $student->SEQNUM_DAY1 = $event;
                $student->EMPLID = (string) $rowData[0][0]; // ambil cel collumn pertama sebagai nim
                //mencari tipe partisipan berdasarkan cel collumn ke 2
                $PARTICIPANT = PsUmnParpntType::findOne(['DESCR' => $rowData[0][1]]);
                if($PARTICIPANT->UMN_PARTICIPANT_ID != null){
                    $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else if ($PARTICIPANT->UMN_PARTICIPANT_ID == null) {
                    $PARTICIPANT = PsUmnParpntType::find()->where(['LIKE', 'DESCR',  $rowData[0][1]])->one();
                    $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else {
                    $student->UMN_PARTICIPANT_ID = 4; 
                }
                //saat memasukkan data baru maka Count Total, UMN points, Attend Present akan di set 0
                // $student->COUNT_TOTAL = 0;
                // $student->UMN_OBTAIN_POINTS = 0;
                // $student->ATTEND_PRESENT = 'N';

                if(!$student->validate()) throw new Exception("Student " .$student->EMPLID." could not be validated ");
                if(!$student->save()) throw new Exception("Student " .$rowData[0][0]. " could not be saved ");
                
            }

            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['ps_umn_stdnt_acvty.SEQNUM_DAY1' => $event]);
            if(Yii::$app->request->post('Save'))
            {
                echo "commit";
                $transaction->commit();
                Yii::info('All Student saved');
            }
            if(Yii::$app->request->post('Cancel'))
            {
                echo "rollBack";
                $transaction->rollBack();
                Yii::info('Import Peserta Canceled');
            }
            return $this->render('ImportTable', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'event' => $SkkmEvent,
                ]);

        } catch (\Exception $e) {
            Yii::$app->session->setFlash('Error',$e->getMessage());
            $transaction->rollBack();
            throw $e;
        } 
        catch (\Throwable $e) {
            Yii::$app->session->setFlash('Error',$e->getMessage());
            $transaction->rollBack();
            throw $e;
        }
    }


    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
            // return $this->goBack();//kembali ke halaman sebelumnya
        return $this->render('ScanQR');
        } 
        else if ($model->load(Yii::$app->request->post()) && !$model->loginAdmin()) {
                Yii::$app->session->setFlash('Error','you are not an Admin');
        }
            
        $model = new LoginForm();
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        if (Yii::$app->user->isGuest) {
        
        $model = new LoginForm();
            return $this->redirect(['login']);
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
