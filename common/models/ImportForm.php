<?php
namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\base\Model;
//use common\models\User;

/**
 * Signup form
 */
class ImportForm extends Model
{
    public $excelFile;
    public $PKey;
    public $nim;
    public $nama;
    public $data=null;
    public $filename = null;
    public $filePath;
    public $fileUrl;
    public $subfolderVar = false;
    public $errorCorrectionLevel = 'L';
    public $matrixPointSize = 5;
    public $displayImage=true;
    public $imageTagOptions=array();
    private $fullUrl;

    public function rules()
    {
        return [
            ['excelFile', 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsm, xlsx, xltx, xltm, xlt, xls, xml'],

            ['matrixPointSize', 'trim'],
            ['matrixPointSize', 'integer', 'min' => 5, 'max' => 10],
            //['matrixPointSize', 'compare', 'compareValue' => 10, 'operator' => '<=', 'type' => 'number'],

        ];
    }
    public function upload()
    {
        if ($this->validate()) { 
            // foreach ($this->excelFile as $file) {
                // $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);
            // }
            $this->excelFile->saveAs('uploads/' . $this->excelFile->baseName . '.' . $this->excelFile->extension);
            return true;
        } else {
            return false;
        }
    }
}
