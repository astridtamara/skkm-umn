<?php
namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\base\Model;
//use common\models\User;

/**
 * Signup form
 */
class BarcodeForm extends Model
{
    public $nim;
    // public $nama;

    public function rules()
    {
        return [
            ['nim', 'trim'],
            ['nim', 'required'],
            ['nim', 'string', 'min' => 5, 'max' => 12],

            // ['nama', 'trim'],
            // ['nama', 'required'],
            // ['nama', 'string', 'min' => 2, 'max' => 50],

        ];
    }
    public function attributeLabels()
    {
        return [
            'nim' => 'NIM Mahasiswa',
        ];
    }
}
