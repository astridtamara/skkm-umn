<?php
namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\base\Model;
//use common\models\User;

/**
 * Signup form
 */
class CreateForm extends Model
{
    public $nim;
    // public $nama;
    public $data=null;
    public $filename = null;
    public $filePath;
    public $fileUrl;
    public $subfolderVar = false;
    public $errorCorrectionLevel = 'L';
    public $matrixPointSize = 5;
    public $displayImage=true;
    public $imageTagOptions=array();
    private $fullUrl;

    public function rules()
    {
        return [
            ['nim', 'trim'],
            ['nim', 'required'],
            ['nim', 'string', 'min' => 5, 'max' => 12],

            // ['nama', 'trim'],
            // ['nama', 'required'],
            // ['nama', 'string', 'min' => 2, 'max' => 50],

            ['matrixPointSize', 'trim'],
            ['matrixPointSize', 'required'],
            ['matrixPointSize', 'integer', 'min' => 5, 'max' => 10],
            //['matrixPointSize', 'compare', 'compareValue' => 10, 'operator' => '<=', 'type' => 'number'],

        ];
    }
    public function attributeLabels()
    {
        return [
            'nim' => 'NIM Mahasiswa',
        ];
    }
}
