<?php
namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\base\Model;
//use common\models\User;

/**
 * Signup form
 */
class ImportPesertaForm extends Model
{
    public $excelFile;
    public $INSTITUTION;
    public $UMN_AREA_ID;
    public $UMN_EVENT_ID;
    public $UMN_LEVEL_ID;
    public $SEQNUM_DAY1;
    public $EMPLID;
    public $UMN_PARTICIPANT_ID;
    public $COUNT_TOTAL;
    public $UMN_OBTAIN_POINTS;
    public $ATTEND_PRESENT;

    private $fullUrl;

    public function rules()
    {
        return [
            ['excelFile', 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsm, xlsx, xltx, xltm, xlt, xls, xml'],
        ];
    }
    public function upload()
    {
        if ($this->validate()) { 
            // foreach ($this->excelFile as $file) {
                // $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);
            // }
            $this->excelFile->saveAs('uploads/' . $this->excelFile->baseName . '.' . $this->excelFile->extension);
            return true;
        } else {
            return false;
        }
    }
}
