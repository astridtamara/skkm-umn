<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    	'defaultRoute' => 'site/index',
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                // 'http://skkm.umn.ac.id/' => 'http://skkm.umn.ac.id/frontend/web/index',
                // 'http://skkm.umn.ac.id/' => 'http://skkm.umn.ac.id/frontend/web/index',
                // 'http://skkm.umn.ac.id/administrator' => 'http://skkm.umn.ac.id/backend/web',
        		'<alias:\w+>' => '/site/<alias>',
                // '<alias:\w+>/<id:\d+>' => '/site/<alias>',
                'skkm/<alias:\w+>' => '/skkm/<alias>',
                // '<alias:\w+>/<id:\d+>' => '/skkm/<alias>',
                'skkm-student/<alias:\w+>' => '/skkm-student/<alias>',
                // '<alias:\w+>/<id:\d+>' => '/skkm-student/<alias>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
    ],
];
// http://localhost/SKKM/advanced/backend/web/index.php?r=site%2Flogin
// http://localhost/SKKM/advanced/backend/web/index.php?r=site%2Fscan
// http://localhost/SKKM/advanced/backend/web/index.php?r=skkm%2Findex
// http://localhost/SKKM/advanced/backend/web/index.php?r=skkm%2Findex&sort=DESCR_80
// http://localhost/SKKM/advanced/backend/web/index.php?SkkmSearch%5BDESCR_80%5D=a&SkkmSearch%5BUMN_AREA_ID%5D=&SkkmSearch%5BUMN_EVENT_ID%5D=&SkkmSearch%5BUMN_LEVEL_ID%5D=&SkkmSearch%5BSTART_DATE%5D=&SkkmSearch%5BEND_DATE%5D=&r=skkm%2Findex&sort=DESCR_80
// http://localhost/SKKM/advanced/backend/web/index.php?r=skkm%2Fview&id=574
// http://localhost/SKKM/advanced/backend/web/index.php?r=site%2Fimport&id=16
// http://localhost/SKKM/advanced/backend/web/index.php?r=site%2Fcreate&id=16
// http://localhost/SKKM/advanced/backend/web/index.php?r=skkm-student%2Findex
// http://localhost/SKKM/advanced/backend/web/index.php?r=skkm-student%2Fcreate
// http://localhost/SKKM/advanced/backend/web/index.php?r=skkm-student%2Fview&SEQNUM_DAY1=16&EMPLID=12110110001
// http://localhost/SKKM/advanced/backend/web/index.php?r=skkm-student%2Fupdate&SEQNUM_DAY1=16&EMPLID=12110110001
// http://localhost/SKKM/advanced/backend/web/index.php?r=site%2Fsignup

// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Findex
// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Fabout
// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Fcontact
// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Fscan
// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Fcreate
// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Fimport
// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Fscanbarcode
// http://localhost/SKKM/advanced/frontend/web/index.php?r=site%2Flogin