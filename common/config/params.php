<?php
return [
    'adminEmail' => 'janssen@student.umn.ac.id',//'student.development@umn.ac.id',
    'adminPlace' => 'Gedung C 206 Ruang Student Development, Universitas Multimedia Nusantara.',// C206 New Media Tower
    'adminTelp' => '(021) 54220808',

    'admin1' => 'Citrandika Krisandua Okta Selarosa, S.Pd., M.A.',
    'admin1Pic' => 'Url::toRoute("assets/QR/image/Staff/CITRA.JPG", true)',
    'admin1Jabatan' => 'Manajer of Internal Student Affairs',
    'admin1Email' => 'citra.selarosa@umn.ac.id',
    'admin1Ext' => '2500',

    'admin2' => 'Ignatius de Loyola Aryo Gurmilang, S.S.',
    'admin2Pic' => 'Url::toRoute("assets/QR/image/Staff/Aryo.JPG", true)',
    'admin2Jabatan' => 'Student Development Officer',
    'admin2Email' => 'aryo.gurmilang@umn.ac.id',
    'admin2Ext' => '2501',

    'admin3' => 'Eugenia Sareba\' sesa, S.E.',
    'admin3Pic' => 'Url::toRoute("assets/QR/image/Staff/Egi.JPG", true)',
    'admin3Jabatan' => 'Student Development Officer',
    'admin3Email' => 'egi.sarebasesa@umn.ac.id',
    'admin3Ext' => '2501',

    'admin4' => 'Andri Seto Baskoro, S.Pd.',
    'admin4Pic' => 'Url::toRoute("assets/QR/image/Staff/Seto.PNG", true)',
    'admin4Jabatan' => 'Student Development Officer',
    'admin4Email' => 'seto.baskoro@umn.ac.id',
    'admin4Ext' => '2501',

    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600, 

];
