<?php
namespace frontend\controllers;

use Yii;
use ZipArchive;
use yii\db\Expression;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use common\widgets\QRCodeGenerator;
use common\models\LoginForm;
use common\models\CreateForm;
use common\models\BarcodeForm;
use common\models\ImportForm;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ContactForm;
use backend\models\PsUmnSkkmEntry;
use backend\models\PsUmnStdntAcvty;
use backend\models\PsUmnStdntVw;
use backend\models\PsUmnEvents;
use backend\models\Panitia;
use backend\models\PsUmnPntPerPct;
use backend\models\PsUmnParpntType;
use backend\models\PsUmnAreas;
// use dosamigos\qrcode\formats\MailTo;
// use dosamigos\qrcode\QrCode;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
       return [
           'access' => [
               'class' => AccessControl::className(),
               'only' => ['logout', 'import', 'importview', 'create', 'scanbarcode'],// hanya dappat diakses oleh user setelah login
               'rules' => [
                   [
                       'actions' => ['login', 'scan', 'scanresult', 'about'],
                       'allow' => true,
                       'roles' => ['?'],
                   ],
                   [
                       'actions' => ['logout', 'import', 'importview', 'create', 'scanbarcode'],
                       'allow' => true,
                       'roles' => ['@'],// hanya dappat diakses oleh user setelah login
                   ],
               ],
           ],
           'verbs' => [
               'class' => VerbFilter::className(),
               'actions' => [
                   'logout' => ['post'],
               ],
           ],
       ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * menampilkan halaman yang akan diakses pertama kali saat membuka web
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    /**
     * menampilkan halaman scan QR
     */
    public function actionScan()
    {
        return $this->render('ScanQR');
    }
    /**
     * menampilkan halaman scan barcode, memproses data dan menampilkan hasilnya
     */
    public function actionScanbarcode()
    {
        if (Yii::$app->user->isGuest) {

        $model = new LoginForm();
            return $this->redirect(['login']);

        }
            $userid =Yii::$app->user->identity->id;
            $Panitia = Panitia::find()
                ->where('UserID = :userid', [':userid' => $userid])
                ->one();
                $SkkmEvent = PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $Panitia->SEQNUM_DAY1])->one();
        $expression = new Expression('NOW()');
        $now = (new \yii\db\Query)->select($expression)->scalar();  // mengambil tanggal hari ini
        // echo $now; // prints the current date
        $model = new BarcodeForm();
        $ena=1;
        if ($SkkmEvent->START_DATE>=$now || $SkkmEvent->END_DATE<=$now) {
                Yii::$app->session->setFlash('error','Belum waktu acara dimulai atau acara telah selesai');
                print_r($now);
                $ena=0;
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $ena==1) {

            if(PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $Panitia->SEQNUM_DAY1,'EMPLID' => $model->nim])->one() != null)
            {
                $StdntAcvty = PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $Panitia->SEQNUM_DAY1,'EMPLID' => $model->nim])->one();
                    $StdntAcvty->COUNT_TOTAL += 1;
                    $StdntAcvty->save();
                $StudentName=PsUmnStdntVw::find()->where(['EMPLID' => $model->nim])->one()->NM;
                Yii::$app->session->setFlash('success',$StudentName.' ('.$model->nim.') HADIR');
            }
            else if(PsUmnStdntVw::find()->where(['EMPLID' => $model->nim])->one()){

                $student = new PsUmnStdntAcvty();
                $student->UMN_AREA_ID = $SkkmEvent->UMN_AREA_ID;
                $student->UMN_EVENT_ID = $SkkmEvent->UMN_EVENT_ID;
                $student->UMN_LEVEL_ID = $SkkmEvent->UMN_LEVEL_ID;
                $student->INSTITUTION = 'UMNKG';
                $student->SEQNUM_DAY1 = $Panitia->SEQNUM_DAY1;
                $student->EMPLID = $model->nim; // ambil cel collumn pertama sebagai nim
                $student->UMN_PARTICIPANT_ID = 4;
                $student->COUNT_TOTAL = 1;
                $StudentName=PsUmnStdntVw::find()->where(['EMPLID' => $model->nim])->one()->NM;

                //menampilkan form peserta on the spot
                return $this->render('AddStudent', [
                        'model' => $student
                    ]);
            }
            else{
                Yii::$app->session->setFlash('error','Mahasiswa dengan NIM '.$model->nim.' tidak ada. ');
            }
        }
        //membersihkan text box nim untuk scan kembali
        $model->nim='';
        return $this->render('ScanBarcode', [
                'model' => $model,
                'ena' => $ena
            ]);
    }

    /**
     * memproses data peserta on the spot
     */
    public function actionOtssave($SEQNUM_DAY1,$EMPLID,$UMN_PARTICIPANT_ID)
    {
         $SkkmEvent = PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $SEQNUM_DAY1])->one();
                $student = new PsUmnStdntAcvty();
                $student->UMN_AREA_ID = $SkkmEvent->UMN_AREA_ID;
                $student->UMN_EVENT_ID = $SkkmEvent->UMN_EVENT_ID;
                $student->UMN_LEVEL_ID = $SkkmEvent->UMN_LEVEL_ID;
                $student->INSTITUTION = $SkkmEvent->INSTITUTION;
                $student->SEQNUM_DAY1 = $SEQNUM_DAY1;
                $student->EMPLID = $EMPLID;
                $student->UMN_PARTICIPANT_ID = $UMN_PARTICIPANT_ID;
                $student->COUNT_TOTAL = 1;
                $StudentName=PsUmnStdntVw::find()->where(['EMPLID' => $student->EMPLID])->one()->NM;
                    if ($student->save()) {
                    Yii::$app->session->setFlash('success',$StudentName.' ('.$student->EMPLID.') HADIR <br>data peserta On The Spot disimpan');
                    }
            return $this->redirect(['scanbarcode']);
    }
    /**
     * tidak jadi menyimpan data peserta on the spot serta memberikan notifikasi dan kembali ke halaman scan barcode
     */
    public function actionCancel($SEQNUM_DAY1,$EMPLID)
    {
        Yii::$app->session->setFlash('error','Mahasiswa dengan NIM '.$EMPLID.' batal didaftarkan ');
            return $this->redirect(['scanbarcode']);
    }
    /**
     * memproses data scan qr dan menampilkan hasilnya
     */
    public function actionScanresult()
    {
        $qrdata = $_GET['data'];
        $b64 = substr($qrdata,6);
        $PKey = substr($qrdata,0,6);
        $b64 = str_replace(array('-','_',' '),array('+','/','='),$b64);
            $encdata=base64_decode($b64);

        $dat = yii::$app->security->decryptByPassword($encdata, $PKey);

        // try {
            $data[] = array_filter(explode('#', $dat));
            // $EMPLID = $data[0][0];
            // $SEQNUM_DAY1 = $data[0][1];
            $EMPLID = isset($data[0][0]) ? $data[0][0] : null;
            $SEQNUM_DAY1 = isset($data[0][1]) ? $data[0][1] : null;
        // } catch (Exception $e) {
        //     Yii::$app->session->setFlash('error','ERROR Scan QR. Silahkan coba lagi. ');
        // }
        if($EMPLID!=null||$SEQNUM_DAY1!=null){
            $Panitia = Panitia::find()
                ->where('PKey = :PKey AND SEQNUM_DAY1 = :SEQNUM_DAY1', [':PKey' => $PKey, 'SEQNUM_DAY1' => $SEQNUM_DAY1])
                ->one();
            $Event = PsUmnSkkmEntry::find()
                ->where('SEQNUM_DAY1 = :SEQNUM_DAY1', [':SEQNUM_DAY1' => $SEQNUM_DAY1])
                ->one();
            $StdntAcvty = PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $SEQNUM_DAY1,'EMPLID' => $EMPLID])->one();
            $UMNArea = PsUmnAreas::find()->where(['UMN_AREA_ID' => $Event->UMN_AREA_ID])->one();
            $Mahasiswa = PsUmnStdntVw::find()
                ->where('EMPLID = :EMPLID', [':EMPLID' => $EMPLID])
                ->one();
            if($qrdata == 'can\'t decryp')
            {
                Yii::$app->session->setFlash('error','ERROR Scan. Silahkan coba lagi. ');
                $ddata=$EMPLID;
            }
            else if($Mahasiswa == null)
            {
                Yii::$app->session->setFlash('error','Mahasiswa dengan NIM ' . $EMPLID . ' tidak ditemukan. ');
                $ddata=$EMPLID;
            }
            else if($Event == null)
            {
                Yii::$app->session->setFlash('error','Event tidak ditemukan. Event = ' . $SEQNUM_DAY1 .'. NIM mahasiswa = ' . $EMPLID);
                $ddata=$EMPLID;
            }
            else if($StdntAcvty != null)
            {
                if ($StdntAcvty->COUNT_TOTAL >=  $Event->COUNT_TOTAL) {
                    $StdntAcvty->ATTEND_PRESENT = 'Y';
                    $StdntAcvty->save();

                    $SKKM = PsUmnPntPerPct::find()->where(['UMN_AREA_ID' => $Event->UMN_AREA_ID,'UMN_EVENT_ID' => $Event->UMN_EVENT_ID,'UMN_LEVEL_ID' => $Event->UMN_LEVEL_ID,'UMN_PARTICIPANT_ID' => $StdntAcvty->UMN_PARTICIPANT_ID])->one();

                    $ddata = [];
                    $ddata['Event'] = $Event->DESCR_80;
                    $ddata['START_DATE'] = $Event->START_DATE;
                    $ddata['END_DATE'] = $Event->END_DATE;
                    $ddata['UMN_PARTICIPANT_ID'] = $StdntAcvty->UMN_PARTICIPANT_ID;
                    $ddata['COUNT_TOTAL'] = $StdntAcvty->COUNT_TOTAL ;
                    $ddata['EMPLID'] = $Mahasiswa->EMPLID;
                    $ddata['NM_MHS'] = $Mahasiswa->NM;
                    $ddata['Point'] = $SKKM->POINTS;
                    $ddata['Area'] = $UMNArea->DESCRFORMAL;
                    Yii::$app->session->setFlash('success','SKKM point sudah masuk. ');
                }
                else{
                    $ddata=$EMPLID;
                    Yii::$app->session->setFlash('error','Absensi kurang. NIM mahasiswa = ' . $EMPLID);
                }
            }
            else{
                Yii::$app->session->setFlash('error','SKKM belum terdaftar. NIM mahasiswa = ' . $EMPLID);
                $ddata=$EMPLID;
            }
        }
        else{
            Yii::$app->session->setFlash('error','ERROR Scan QR. Silahkan coba lagi. ');
            $ddata=$dat;
        }
        return $this->render('ScanQRresult', [
                'ddata' => $ddata,
            ]);
    }
    /**
     * menampilkan halaman Create QR
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {

        $model = new LoginForm();
             return $this->redirect(['login']);
        }
        $userid =Yii::$app->user->identity->id;
        $Panitia = Panitia::find()
            ->where('UserID = :userid', [':userid' => $userid])
            ->one();
        Yii::$app->session->set('PKey', $Panitia->PKey);
        $model = new CreateForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if((PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $Panitia->SEQNUM_DAY1,'EMPLID' => $model->nim])->one() == null) && (PsUmnStdntVw::find()->where(['EMPLID' => $model->nim])->one()))
            {
                $SkkmEvent = PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $Panitia->SEQNUM_DAY1])->one();
                    $student = new PsUmnStdntAcvty();
                    $student->UMN_AREA_ID = $SkkmEvent->UMN_AREA_ID;
                    $student->UMN_EVENT_ID = $SkkmEvent->UMN_EVENT_ID;
                    $student->UMN_LEVEL_ID = $SkkmEvent->UMN_LEVEL_ID;
                    $student->INSTITUTION = $SkkmEvent->INSTITUTION;
                    $student->SEQNUM_DAY1 = $Panitia->SEQNUM_DAY1;
                    $student->EMPLID = $model->nim; // ambil cel collumn pertama sebagai nim

                $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID])->one();

                if($PARTICIPANT != null){
                        $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else if ($PARTICIPANT->UMN_PARTICIPANT_ID == null) {
                    $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID])->one();
                    if($PARTICIPANT != null){
                        $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                    }
                    else{
                        $student->UMN_PARTICIPANT_ID = 4;
                    }
                }
                //$student->UMN_PARTICIPANT_ID = 4;
                // print_r($student);// menampilkan data student
                if(!$student->validate()) throw new Exception("Student " .$student->EMPLID." could not be validated ");
                if(!$student->save()) throw new Exception("Student " .$model->nim. " could not be saved ");

            }
            if(PsUmnStdntVw::find()->where(['EMPLID' => $model->nim])->one()){
                $encdata = Yii::$app->security->encryptByPassword($model->nim.'#'.$Panitia->SEQNUM_DAY1, $Panitia->PKey);
                $b64=base64_encode($encdata);
                $b64 = str_replace(array('+','/','='),array('-','_',' '),$b64);
                QRCodeGenerator::widget([
                            'data' => $Panitia->PKey.$b64,
                            'filename' => $model->nim . ".png",
                            'subfolderVar' => false,
                            'displayImage'=>true,
                            'errorCorrectionLevel' => 'L',
                            'matrixPointSize'=> 7// 1 to 10 only
                            ]);
                Yii::$app->session->setFlash('success','QRCode Created');
            }
            else{
                Yii::$app->session->setFlash('error', 'Mahasiswa tidak ditemukan');
            }
        } else {
                // Yii::$app->session->setFlash('error', 'There was an error.');
        }
        return $this->render('CreateQR', [
                'model' => $model,
            ]);
    }

    /**
     * memasukan seluruh file image QR yang telah dibuat kedalam zip
     */
    private static function folderToZip($folder, &$zipFile, $exclusiveLength) {
        $handle = opendir($folder);
        while (false !== $f = readdir($handle)) {
          if ($f != '.' && $f != '..') {
            $filePath = "$folder/$f";
            // Remove prefix from file path before add to zip.
            $localPath = substr($filePath, $exclusiveLength);
            if (is_file($filePath)) {
              $zipFile->addFile($filePath, $localPath);
            }
            elseif (is_dir($filePath)) {
              // Add sub-directory.
              $zipFile->addEmptyDir($localPath);
              self::folderToZip($filePath, $zipFile, $exclusiveLength);
            }
          }
        }
        closedir($handle);
    }

    /**
     * mempersiapkan data yang akan di zip
     */
    public static function zipDir($sourcePath, $outZipPath) {
        $pathInfo = pathInfo($sourcePath);
        $parentPath = $pathInfo['dirname'];
        $dirName = $pathInfo['basename'];

        $z = new ZipArchive();
        $z->open($outZipPath, ZIPARCHIVE::CREATE);
        $z->addEmptyDir($dirName);
        //memasukan seluruh file image QR yang telah dibuat kedalam zip
        self::folderToZip($sourcePath, $z, strlen("$parentPath/"));
        $z->close();
    }

    /**
     * mendownload seluruh file image QR yang telah dibuat
     */
    public function actionDownload()
    {
        if (Yii::$app->user->isGuest) {
            $model = new LoginForm();
            return $this->redirect(['login']);
        }
        $dir=realpath(Yii::$app->basePath.'/web/assets/QR/temp/').'/'.Yii::$app->user->identity->username;

        if(file_exists($dir))
        {
            //menzip data
            self::zipDir($dir, $dir.'.zip');

            $filesdel = array_diff(scandir($dir), array('.','..'));
            foreach ($filesdel as $file) {
              (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
            }
            rmdir($dir);
        }
        if(file_exists($dir.'.zip'))
        {
          Yii::$app->response->sendFile($dir.'.zip');
        }
        else{throw new CHttpException(404, 'The requested file does not exist.');}
        // unlink($dir.'.zip');
    }

    /**
     * menampilkan halaman Import excel untuk membuat QR Code
     */
    public function actionImport()
    {
        if (Yii::$app->user->isGuest) {

        $model = new LoginForm();
            return $this->redirect(['login']);
        }
        $userid =Yii::$app->user->identity->id;
        $Panitia = Panitia::find()
            ->where('UserID = :userid', [':userid' => $userid])
            ->one();
        Yii::$app->session->set('PKey', $Panitia->PKey);

        $model = new ImportForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->excelFile = UploadedFile::getInstance($model, 'excelFile');
            if ($model->upload()) {
            return $this->redirect(['importview',
                'excelFile' => $model->excelFile->name,
                'errorCorrectionLevel' => 'L',
                'matrixPointSize'=> 7,
                // 'errorCorrectionLevel' => $model->errorCorrectionLevel,
                // 'matrixPointSize'=> $model->matrixPointSize
                'Event' => $Panitia->SEQNUM_DAY1
                ]);
            }
        }
        return $this->render('Import', [
                'model' => $model,
            ]);
    }

    /**
     * menampilkan halaman hasil create QR Code dari Import excel
     */
    public function actionImportview($excelFile,$errorCorrectionLevel,$matrixPointSize,$Event)
    {
        if (Yii::$app->user->isGuest) {
        $model = new LoginForm();
            return $this->redirect(['login']);
        }
        $PKey = Yii::$app->session->get('PKey');
        $inputFile = 'uploads/' . $excelFile;
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFile);
        } catch (Exception $e) {
            die('Error');
        }
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row=1; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null,true,false);
            if($row==1 || $rowData[0][0] == '')
            {
                continue;
            }
            $string = (string) $rowData[0][0] ;

            if((PsUmnStdntAcvty::find()->where(['SEQNUM_DAY1' => $Event,'EMPLID' => $rowData[0][0]])->one() == null)&&(PsUmnStdntVw::find()->where(['EMPLID' => $rowData[0][0]])->one()))
            {
                $SkkmEvent = PsUmnSkkmEntry::find()->where(['SEQNUM_DAY1' => $Event])->one();
                    $student = new PsUmnStdntAcvty();
                    $student->UMN_AREA_ID = $SkkmEvent->UMN_AREA_ID;
                    $student->UMN_EVENT_ID = $SkkmEvent->UMN_EVENT_ID;
                    $student->UMN_LEVEL_ID = $SkkmEvent->UMN_LEVEL_ID;
                    $student->INSTITUTION = $SkkmEvent->INSTITUTION;
                    $student->SEQNUM_DAY1 = $Event;
                    $student->EMPLID = $string; // ambil cel collumn pertama sebagai nim

                $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID,'DESCR' => $rowData[0][1]])->one();
                if($PARTICIPANT != null){
                        $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else if ($PARTICIPANT->UMN_PARTICIPANT_ID == null) {
                    $PARTICIPANT = PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $SkkmEvent->UMN_AREA_ID,'UMN_EVENT_ID' => $SkkmEvent->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $SkkmEvent->UMN_LEVEL_ID])->one();
                    $student->UMN_PARTICIPANT_ID = $PARTICIPANT->UMN_PARTICIPANT_ID;
                }
                else {
                    $student->UMN_PARTICIPANT_ID = 4;
                }
                // print_r($student);
                if(!$student->validate()) throw new Exception("Student " .$student->EMPLID." could not be validated ");
                if(!$student->save()) throw new Exception("Student " .$string. " could not be saved ");

            }

            if(PsUmnStdntVw::find()->where(['EMPLID' => $string])->one()){
                $encdata = Yii::$app->security->encryptByPassword($string.'#'.$Event, $PKey);
                $b64=base64_encode($encdata);
                $b64 = str_replace(array('+','/','='),array('-','_',' '),$b64);
                QRCodeGenerator::widget([
                            'data' => $PKey.$b64,
                            'filename' => $string . ".png",
                            'subfolderVar' => Yii::$app->user->identity->username,
                            'displayImage'=>true,
                            'errorCorrectionLevel'=> $errorCorrectionLevel,
                            'matrixPointSize'=> $matrixPointSize, // 1 to 10 only
                            ]);
                $students[] = array('nim'=>(string)$rowData[0][0]);
            }
            else{
                $students[] = array('nim'=> (string)$rowData[0][0]);
            }
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $students,
            'sort' => [
                'attributes' => ['nim'],
            ],
        ]);
        return $this->render('ImportTable', [
                'dataProvider' => $dataProvider,
            ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        else if ($model->load(Yii::$app->request->post())) {
                Yii::$app->session->setFlash('Error','Login Gagal atau anda bukan Panitia');
        }

        $model = new LoginForm();
            return $this->render('login', [
                'model' => $model,
            ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
