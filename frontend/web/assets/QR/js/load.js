/*
Slider ini menggunakan slider open source
dari BXslider. Modul untuk slider pada 
tranyar.co.id dimodifikasi oleh pihak
internal dari tranyar.co.id dan disesuaikan
untuk kebutuhan tranyar.co.id.
*/

// require('@web/assets/QR/js/jquery.bxslider.min.js');\
// $this->registerJsFile('@web/assets/QR/js/jquery.bxslider.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$(document).ready(function() {	

		$('.bxslider').bxSlider({
		//jumlah gambar minimum yang tampil
		minSlides: 5,
		//jumlah maksimum gambar yang tampil
  		maxSlides: 12,
  		//besaran panjang gambar dalam pixel
  		slideWidth: 150,
  		//margin antar gambar
  		slideMargin: 15,
  		//efek untuk autoplay seperti neswsticker
  		ticker: true,
  		//kecepatan makin besar angkanya makin pelan slidernya
	  	speed: 35000
		});
});