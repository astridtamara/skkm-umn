<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
        use yii\helpers\Url;
use backend\models\PsUmnAreas;
use backend\models\PsUmnEvents;
use backend\models\PsUmnLevels;
use backend\models\PsUmnSkkmEntry;
use backend\models\PsUmnStdntVw;
use backend\models\PsUmnParpntType;

/* @var $this yii\web\View */
/* @var $model backend\models\UMNStudentSKKM */

$this->title = 'Add UMN Student SKKM On The Spot';
?>
<div class="umnstudent-skkm-update">

    <h1><?= Html::encode($this->title) ?></h1>

      <div class="umnstudent-skkm-form">

          <?php $form = ActiveForm::begin(); ?>

          <?= $form->field($model, 'SEQNUM_DAY1')->dropDownList(
                  ArrayHelper::map(PsUmnSkkmEntry::find()->orderBy(['DESCR_80'=>SORT_ASC])->all(),'SEQNUM_DAY1','DESCR_80'),
                  ['class'=>'form-control event','prompt'=>'Select Event','disabled' =>  true]
          ) ?>

          <?= $form->field($model, 'EMPLID')->textInput(['type' => 'number','maxlength' => true,'disabled' => true])?>
           
          <?= $form->field($model,'Nama')->textInput(['type' => 'text', 'disabled' => true, 'value' => ($model->mahasiswa->NM)])->label('Nama Mahasiswa')?>

          <?= $form->field($model, 'UMN_PARTICIPANT_ID')->dropDownList(
                  ArrayHelper::map(PsUmnParpntType::find()->joinWith(['psUmnStdntAcvties'])->orderBy(['DESCR'=>SORT_ASC])->where(['UMN_AREA_ID' => $model->UMN_AREA_ID,'UMN_EVENT_ID' => $model->UMN_EVENT_ID, 'UMN_LEVEL_ID' => $model->UMN_LEVEL_ID])->all(),'UMN_PARTICIPANT_ID','DESCR'),
                  ['class'=>'form-control participant','prompt'=>'Select Participant']
          ) ?>

          <div class="form-group">
              <!-- ?= Html::submitButton('Cancel', ['name' => 'Cancel', 'value' => 'Cancel', 'class' => 'btn btn-danger']) ?>
              ?= Html::submitButton('Save', ['name' => 'Save', 'value' => 'Save', 'class' => 'btn btn-success']) ?> -->
        <?= Html::a('Cancel', ['cancel', 'SEQNUM_DAY1' => $model->SEQNUM_DAY1,'EMPLID' => $model->EMPLID], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Save', ['otssave', 'SEQNUM_DAY1' => $model->SEQNUM_DAY1,'EMPLID' => $model->EMPLID,'UMN_PARTICIPANT_ID' => $model->UMN_PARTICIPANT_ID], ['class' => 'btn btn-success']) ?>
          </div>

          <?php ActiveForm::end(); ?>

      </div>

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
