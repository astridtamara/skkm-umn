<?php

/* @var $this yii\web\View */

$this->title = 'Student Development UMN | Universitas Multimedia Nusantara';
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('assets/img/lib-bg.jpg')">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="width:100vw; ">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src='assets/img/college-bg.jpg' alt="First slide" style="height:45vw;object-fit: cover;">
        <div class="carousel-caption d-none d-md-block">
          <h5>SKKM Pengabdian Masyarakat</h5>
          <p>Berkontribusi demi meningkatkan kesejahterahan masyarakat</p>
        </div>
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src='assets/img/work-bg.jpg' alt="Second slide" style="height:45vw;object-fit: cover;">
        <div class="carousel-caption d-none d-md-block">
          <h5>SKKM Pengabdian Masyarakat</h5>
          <p>Berkontribusi demi meningkatkan kesejahterahan masyarakat</p>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src='assets/img/lib-bg.jpg' alt="Third slide" style="height:45vw;object-fit: cover;">
        <div class="carousel-caption d-none d-md-block">
          <h5>SKKM Pengabdian Masyarakat</h5>
          <p>Berkontribusi demi meningkatkan kesejahterahan masyarakat</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</header>

<div class="site-index">
  <div class="body-content">
      <div class="row">   
      </div>
  </div>
</div>

  <?php if(Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
    <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
  <?php endif; ?>

</div>