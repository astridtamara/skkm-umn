<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Student Development';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Page Header -->
<header class="masthead" style="background-image: url('assets/img/about-bg.jpg')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h1>About</h1>
          <h2 class="subheading">We are Student Development Team!</h2>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="container">
	<div class="site-about">

	    <div class="row">
	        <div class="col-lg-4 center">
	            <?= Html::img(Url::toRoute('assets/QR/image/Logo Student Dev UMN.png', true)  , ['class' => 'img-responsive center']); ?>
	        </div>
	        <div class="col-lg-8">
		    	<div class="row">
		    		<h3 align="justify">Peran Bidang Pengembangan <i>Softskills</i></h3>
					<p align="justify">Bidang Pengembangan <i>Softskills</i> di Universitas Multimedia Nusantara berperan dalam mengembangkan kecakapan lunak (<i>soft skills</i>) setiap mahasiswa. Kecakapn lunak merupakan atribut pribadi yang merefleksikan kemampuan emosional (<i>Emotional Intellegence Quotient</i>) yang meliputi:</p>
					<ol align="justify">
						<li>Kemampuan kepemimpinan, seperti kemampuan mengambil keputusan dalam situasi sulit dan kritis, kemampuan mengelola dan mengarahkan orang (<i>people management</i>); </li>
						<li>Kemampuan berpendapat, bernegosiasi, dan beragumentasi secara persuasif;</li>
						<li>Kemampuan mendengarkan pendapat orang lain secara empatik;</li>
						<li>Kemampuan bekerja sama dalam tim dan bersinergi;</li>
						<li>Kemampuan membagi waktu secara berkualitas;</li>
						<li>Kemampuan kerja yang dapat diandalkan sesuai dengan fungsinya dalam struktur organisasi;</li>
						<li>Kemampuan menerima kritik dan mentransformasi diri; dan </li>
						<li>Kemampuan merancang, melaksanakan, dan membuat kegiatan kemahasiswaan secara transparan dan bertanggung jawab.</li>
					</ol>
					<p align="justify">Poin-poin di atas menjadi indikator dalam melihat perkembangan <i>softskills</i> dari setiap mahasiswa, sekaligus juga menjadi ajang latihan untuk menjadi lulusan yang berwatak baik dan profesional.</p>
				</div>
			</div>
		</div>

	    <div class="row">
	        <div class="col-lg-12">
		    	<div class="row">
		    		<h3 align="justify">Program Kerja Bidang Pengembangan <i>Softskills</i></h3>
					<ol align="justify">
						<li>Menyelenggarakan Pelatihan <i>Leadershiip and Teamworking</i> yang wajib diikuti oleh mahasiswa baru pada setiap semester genap.</li>
						<li>Mengelola penyelenggaraan kegiatan mahasiswa yang dilaksanakn oleh organisasi-organisasi kemahasiswaan.</li>
						<li>Menggelar Latihan Dasar Kepemimpinan (LDK), dengan model <i>outbound</i> di luar kampus untuk para pengurus BEM, KBM, Himpunan Mahasiswa Prodi, dan UKM yang dilaksanakan satu tahun sekali.</li>
						<li>Pengadaan seminar dengan tema-tema seputar <i>softskills</i> yang dilaksanakan setiap semester.</li>
						<li>Visitasi dan studi banding ke universitas-universitas lain.</li>
						<li>Orientasi Mahasiwa Baru menjelang dimulainya perkuliahan tahun ajaran baru yang dilaksanakan setahun sekali.</li>
						<li>Wisuda sekaligus Dies Natalis UMN yang dilaksanakan setahun sekali.</li>
					</ol>
				</div>
			</div>
		</div>

	    <div class="row">
	        <div class="col-lg-8">
		    	<div class="row">
		    		<h3 align="justify">Contact</h3>
		        	<div class="col-lg-2">
						<p align="left">Phone</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['adminTelp'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">E-mail</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['adminEmail'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Place</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['adminPlace'];?></p>
					</div>
				</div>
			</div>
		</div>

	    <div class="row">
	        <div class="col-lg-4 center">
	            <?= Html::img(Url::toRoute("assets/QR/image/Staff/CITRA.JPG", true) , ['class' => 'img-responsive center','width' => '150px']); ?>
	        </div>
	        <div class="col-lg-8">
		    	<div class="row">
		    		<h3 align="left"><?= Yii::$app->params['admin1Jabatan'];?></h3>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Name</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin1'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">E-mail</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin1Email'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Extention</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin1Ext'];?></p>
					</div>
				</div>
	        </div>
		</div>

	    <div class="row">
	        <div class="col-lg-4 center">
	            <?= Html::img(Url::toRoute("assets/QR/image/Staff/Aryo.JPG", true) , ['class' => 'img-responsive center','width' => '150px']); ?>
	        </div>
	        <div class="col-lg-8">
		    	<div class="row">
		    		<h3 align="left"><?= Yii::$app->params['admin2Jabatan'];?></h3>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Name</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin2'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">E-mail</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin2Email'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Extention</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin2Ext'];?></p>
					</div>
				</div>
	        </div>        
		</div>

	    <div class="row">
	        <div class="col-lg-4 center">
	            <?= Html::img(Url::toRoute("assets/QR/image/Staff/Egi.JPG", true) , ['class' => 'img-responsive center','width' => '150px']); ?>
	        </div>
	        <div class="col-lg-8">
		    	<div class="row">
		    		<h3 align="left"><?= Yii::$app->params['admin3Jabatan'];?></h3>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Name</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin3'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">E-mail</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin3Email'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Extention</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin3Ext'];?></p>
					</div>
				</div>
	        </div>
		</div>

	    <div class="row">
	        <div class="col-lg-4 center">
	            <?= Html::img(Url::toRoute("assets/QR/image/Staff/Seto.png", true), ['class' => 'img-responsive center','width' => '150px']); ?>
	        </div>
	        <div class="col-lg-8">
		    	<div class="row">
		    		<h3 align="left"><?= Yii::$app->params['admin4Jabatan'];?></h3>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Name</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin4'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">E-mail</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin4Email'];?></p>
					</div>
				</div>
		    	<div class="row">
		        	<div class="col-lg-2">
						<p align="left">Extention</p>
					</div>
		        	<div class="col-lg-10">
						<p align="left"> : <?= Yii::$app->params['admin4Ext'];?></p>
					</div>
				</div>
	        </div>
	    </div>
	</div>

</div>
