<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\CreateForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
    use kartik\grid\GridView;
// use common\widgets\QRCodeGenerator;
// use yii\widgets\HelloWidget;

$this->title = 'Create QR Code';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="site-create">
    <h1><?= Html::encode($this->title) ?></h1>

            <?= Html::a('Import Excel', ['import'], ['class' => 'btn btn-success']) ?>
            
    <p>Please fill out the following fields:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'Create-form']); ?>

                <?= $form->field($model, 'nim')->textInput(['type' => 'number'])->label('NIM Mahasiswa')?>

                
                <!-- $form->field($model, 'nama')->textInput(['autofocus' => true])
                

                $form->field($model, 'errorCorrectionLevel') 
                    ->dropDownList(
                        ['L' => 'L - smallest',
                            'M' => 'M',
                            'Q' => 'Q',
                            'H' => 'H - best']
                    );

                $form->field($model, 'matrixPointSize')->textInput(['type' => 'number']) -->

                <div class="form-group">
                    <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
			<!--</div>-->
			<div id="qrimage" class="center">
            <?php
                if(Yii::$app->session->hasFlash('success')){
                    echo Html::img('@web/assets/QR/temp/'.$model->nim.'.png' , ['class' => 'img-responsive center']);
                }
            ?>
                    <?php  ?>
			</div>
    </div>
     <!-- Html::a('CreateTable', ['import', 'errorCorrectionLevel' => $model->errorCorrectionLevel, 'matrixPointSize'=> $model->matrixPointSize], ['class' => 'btn btn-primary'])  -->
 
     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>

</div>
