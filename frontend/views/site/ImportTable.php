<?php

use yii\helpers\Html;
// use yii\widgets\DetailView;
use yii\grid\GridView;
use kartik\icons\Icon;
use backend\models\PsUmnStdntVw;
// use app\controllers\SiteController; 


// Initialize a specific framework - e.g. Web Hosting Hub Glyphs 
                            Icon::map($this, Icon::BSG);
/* @var $this yii\web\View */
/* @var $model backend\models\UMNSkkmEntry */
// $this->title = $model->SEQNUM_DAY1;
$this->title = 'Create QR Code';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-create">
    <h1><?= Html::encode($this->title) ?></h1>
        <?= Html::a('Download QR Code', ['download'], ['class' => 'btn btn-primary']) ?>

<!-- echo SiteController::helloWorld();  -->
    <?=
    // $this->context->ImportExcel();
        GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'nim',
            ],
            // 'nama',
            [
                'attribute' => 'nim',
                'format' => 'html',
                'label' => 'QR Code',
                'value' => function ($data) {
                    if(PsUmnStdntVw::find()->where(['EMPLID' => $data['nim']])->one()){
                        return Html::img('@web/assets/QR/temp/'.Yii::$app->user->identity->username.'/'. $data['nim'].'.png' , ['class' => 'img-responsive center','width' => '60px']);
                    }
                    else {
                        return Icon::show('remove fa-4x center', ['aria-hidden'=>"true",'style'=>"color:red;text-shadow:2px 2px 4px #000000;"], Icon::BSG);
                    }
                },
            ],
        ],
    ]);
    // foreach ($dataProvider as $key) {
    //     echo $key."<br>";
    // }
    // print_r($dataProvider);
            ?>

     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
