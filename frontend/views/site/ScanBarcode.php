<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;


$this->title = 'Scan Barcode';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="site-scanbarcode">
    <h1><?= Html::encode($this->title) ?></h1>
	<div class="row center" id="mainbody"> 
    <?php 
      Modal::begin([
        'header'=>'<h1>On The Spot</h1>',
        'id' => 'modal',
        'size' => 'modal-lg',
        "footer"=>
            Html::a('Close', ['#'],
            ['title' => 'Close', 'class' => 'btn btn-default pull-left', 'data-dismiss'=>"modal"]) .
            Html::a('ОК', ['#'],
            ['title' => 'Delete', 'class' => 'btn btn-primary', 'id' => 'delete-confirm-btn']),
        ]);
      echo "<div id='modalContent'> </div>";
      Modal::end();
    ?>

      <div class="col-lg-5">
        <?php if ($ena==1): ?>
          <?php $form = ActiveForm::begin(['id' => 'Barcode-form']); ?>
			        <?= $form->field($model, 'nim')->textInput(['type' => 'number','autofocus' => true])->label('NIM Mahasiswa') ?>
              <div class="form-group">
                  <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
              </div>
          <?php ActiveForm::end(); ?>
        <?php endif; ?>
      </div>
	</div>
     <?php if (Yii::$app->session->hasFlash('Error')): ?>
  <div class="alert alert-danger alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <!-- <h4><i class="icon fa fa-check"></i>Saved!</h4> -->
  <strong>Warning!</strong>  <?= Yii::$app->session->getFlash('Error') ?>
  </div>
<?php endif; ?>
</div>
</div>