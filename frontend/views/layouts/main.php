<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
// use frontend\assets\AppQRAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
     <?php $this->head() ?> 
    <meta content="Indonesia" name="geo.placename"/>
    <meta content="university, universitas, multimedia, nusantara, serpong, gading, tangerang, kompas, gramedia, online" name="KEYWORDS"/>
    <meta name="description" content="Universitas Multimedia Nusantara">
    <meta name="author" content="universitas multimedia nusantara">
    <!-- Icon-->
    <link rel="shortcut icon" href="<?php echo Url::toRoute('assets/QR/image/logo-umn.png', true)?>" type="image/png" >
    <!-- Custom fonts for this template -->
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <link rel="shortcut icon" href="<?php //echo Yii::$app->request->baseUrl; ?>/assets/QR/image/Logo Student Dev UMN.png" type="image/png" > -->
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56c18d73f0b07e75" async="async"></script> -->
</head>
<body>
<?php $this->beginBody() ?>

    <!-- Navigation -->
    <div class="wrap">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
          <div class="container">
            <a class="navbar-brand" href="<?php echo Yii::$app->homeUrl; ?>">Student Development UMN</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              Menu
              <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo Url::home(); ?>">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo Url::to(['site/about']); ?>">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo Url::to(['site/contact']); ?>">Contact</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo Url::to(['site/scan']); ?>">Scan QR Code</a>
                </li>
                <?php
                    if (Yii::$app->user->isGuest) {
                        echo '<li class="nav-item">'.
                          '<a class="nav-link" href="'.Url::to(['site/login']).'">Login</a>'.
                        '</li>';
                    }
                    else{
                        echo '<li class="nav-item">'.
                          '<a class="nav-link" href="'.Url::to(['site/create']).'">Create QR Code</a>'.
                        '</li>';
                        echo '<li class="nav-item">'.
                          '<a class="nav-link" href="'.Url::to(['site/scanbarcode']).'">Presensi</a>'.
                        '</li>';
                        '<li>'
                        . Html::beginForm(Url::to(['/site/logout']), 'post')
                        . Html::submitButton(
                            'Logout<br>(' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>';
                    }
                ?>
              </ul>
            </div>
          </div>
        </nav>
    </div>

    <!-- BODY CONTENT -->
    <!-- <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?> -->
    <?= Alert::widget() ?> 
    <?= $content ?>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-md-12 mx-auto">
            <div class="row">
              <div class="col-lg-2 col-md-2 text-center">
                  <?= Html::img(Url::toRoute('assets/QR/image/Logo Student Dev UMN.png', true) , ['class' => 'img-responsive center','width' => '100px']); ?>
              </div>
              <div class="col-lg-3 col-md-4">
                  <p class="text-muted">&copy; Universitas Multimedia Nusantara <?= date('Y') ?>. All Rights Reserved.</p>
              </div>
              <div class="col-lg-5 col-md-6">
                  <p class="text-muted"><?= Yii::$app->params['adminPlace'];?><br/>
                  Jl. Boulevard Gading Serpong, Tangerang, Banten, Indonesia<br/>
                  (T)+62 21 5422 0808; (F)+62 21 5422 0800<br/>
                  </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
