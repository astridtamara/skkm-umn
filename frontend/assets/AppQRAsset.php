<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppQRAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'assets/QR/css/bootstrap.css',
        // 'assets/QR/css/bootstrap.css%20[sm]',
        // 'assets/QR/css/main.css',
        // 'assets/QR/css/master.css',
        'assets/QR/css/style.css',
        // 'assets/QR/css/less/',        
    ];
    public $js = [
        // 'assets/QR/js/bootstrap.min.js',
        'assets/QR/js/coda.js',
        'assets/QR/js/jquery-2.1.1.js',
        'http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56c18d73f0b07e75',
        'assets/QR/js/llqrcode.js',
        'assets/QR/js/load.js',
        'assets/QR/js/webqr.js',
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $publishOptions = [
        // 'only' => [
        //     'assets/QR/',
        // ]
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
