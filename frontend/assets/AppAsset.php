<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/clean-blog.css',
        'vendor/bootstrap/css/bootstrap.css',
        'vendor/font-awesome/css/font-awesome.css'
    ];
    public $js = [
        'vendor/jquery/jquery.min.js',
        'vendor/bootstrap/js/bootstrap.bundle.js',
        'js/main.js',
        'js/clean-blog.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
